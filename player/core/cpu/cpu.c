/*****************************************************************************
 *
 * This program is free software ; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * $Id: cpu.c 585 2006-01-16 09:48:55Z picard $
 *
 * The Core Pocket Media Player
 * Copyright (c) 2004-2005 Gabor Kovacs
 *
 ****************************************************************************/

#include "../common.h"
#include "cpu.h"

#ifdef ARM
extern int STDCALL CheckARM5E();
extern int STDCALL CheckARMXScale();
#endif

#ifndef SH3
extern void STDCALL GetCpuId(int,uint32_t*);

#if defined(TARGET_SYMBIAN) && !defined(ARM)
#define GetCpuId(a,b)
#endif

static NOINLINE void SafeGetCpuId(int Id, uint32_t* p)
{
	memset(p,0,4*sizeof(uint32_t));
	TRY_BEGIN
	{
		bool_t Mode = KernelMode(1);
		GetCpuId(Id,p); 
		KernelMode(Mode);
	}
	TRY_END
}
#endif

int CPUCaps()
{
	cpudetect p;
	CPUDetect(&p);
	return p.Caps;
}

void CPUDetect(cpudetect* p)
{
	int Caps = 0;
	//uint32_t CpuId[4];
	memset(p,0,sizeof(cpudetect));

	p->Arch = T("ARM");

	p->Caps = Caps;
}

