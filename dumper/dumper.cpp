// dumper.cpp : Defines the entry point for the application.
//

#include "dumper.h"
#include "MainFrame.h"
#include "mmc_can.h"

IMPLEMENT_APP(Dumper);

  bool Dumper::OnInit () {
    ::wxInitAllImageHandlers();

    WCHAR tmp[1] = {0};
    
    this->m_logging = false;
    this->m_nmeaMapping = false;

    ::wxLog::SetActiveTarget(new ::wxLogStream(&std::cout));

    this->fr = new MainFrame();
    fr->Show();
    //fr->SetPosition(wxPoint(100, 100));
    //fr->SetSize(wxSize(500, 250));

    wxTextCtrl* tc = new wxTextCtrl(fr, wxID_ANY, wxT("Log...\n"), wxPoint(0, 0), wxSize(400, 250), wxTE_MULTILINE|wxTE_LEFT|wxTE_READONLY);
    SetWindowLong((HWND)tc->GetHWND(), GWL_STYLE, ES_MULTILINE | ES_READONLY);
    ::wxLog::SetActiveTarget(new wxLogTextCtrl(tc));

    wxButton* bExit = new wxButton(fr, wxID_EXIT, wxT("Exit"), wxPoint(400, 220), wxSize(100, 30));
    this->m_bLogToggle = new wxButton(fr, ::wxID_OPEN, wxT("Start Log"), wxPoint(400, 0), wxSize(100, 30));
    this->m_bNMEAToggle = new wxButton(fr, ::wxID_CLOSE, wxT("Start NMEA"), wxPoint(400, 30), wxSize(100, 30));
    
    CHAR w[15] = {0x0};
    w[1] = 0x4c; w[3] = 0x6f;

    ::wxLogMessage(wxT("TT: %s"), wxString(w, ::wxMBConvUTF16BE(),8));

    wxLogMessage(wxT("Ready"));
    
    this->m_bytes = new wxStaticText(fr, wxID_ANY, wxT("0KB"), wxPoint(430, 85));
    
    m_HWND = ::CreateWindowEx(0, L"Static", L"CanHandlerMessageWindow", 0, 0, 0, 0, 0, 0, NULL, ::wxGetInstance(), NULL);
    ::SetWindowLong(m_HWND, GWL_WNDPROC, (long)&Dumper::Receiver);
    ::SetWindowLong(m_HWND, GWL_USERDATA, (long)this);
    
    //wxLogMessage(wxT("CAN: Init res %d"), CAN_ERR_NONE == ::CAN_Init(m_HWND));
    wxLogMessage(wxT("BT: Open res: %d"), ::BT_NFORE_Open(m_HWND));
    wxLogMessage(wxT("BT: Log init: %d"), ::BT_NFORE_LogStart("\\StaticStore\\btlog.txt"));
    return true;
  }
  
  void Dumper::LogToggle () {
    if (!this->m_logging) {
      #ifdef _DEBUG
      wxLogMessage(wxT("Log start: %d"), this->m_logging = CAN_ERR_NONE == ::CAN_LogStart("\\StaticStore\\can.dat"));
      #else
      wxLogMessage(wxT("Log start: %d"), this->m_logging = CAN_ERR_NONE == ::CAN_LogStart("\\SDMMC\\can.dat"));
      #endif;
    }
    else {
      wxLogMessage(wxT("Stop log"));
      this->m_logging = !(CAN_ERR_NONE == ::CAN_LogStop());
    }
    this->m_bLogToggle->SetLabel(this->m_logging ? wxT("Stop log") : wxT("Start log"));
  }
  
  void Dumper::NMEAToggle () {
    if (!this->m_nmeaMapping) {
      wxLogMessage(wxT("VCOM start: %d"), this->m_nmeaMapping = CAN_ERR_NONE == ::CAN_VCOMInit());
    }
    else {
      wxLogMessage(wxT("VCOM stop"));
      this->m_nmeaMapping = !(CAN_ERR_NONE == ::CAN_VCOMClose());
    }
    this->m_bNMEAToggle->SetLabel(this->m_nmeaMapping ? wxT("Stop NMEA") : wxT("Start NMEA"));
  }
  
  LRESULT CALLBACK Dumper::Receiver (HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
    Dumper* d = (Dumper*)GetWindowLong(hwnd, GWL_USERDATA);
    NFORE_CMD c = {0};
    static size_t bytes = 0;
    switch (msg) {
      case WM_CAN_RUNNING_STATE:
        ::wxLogMessage(_T("Running state: %d"), wParam);
        break;
      case WM_CAN_VCOM_DEV:
        if (wParam == 0) {
          ::wxLogMessage(_T("VCOM Status: %d, err: %d"), wParam, lParam);
        }
        else {
          ::wxLogMessage(_T("VCOM Status: %d, idx: %d"), wParam, lParam);
        }
        break;
      case WM_CAN_LOG_BYTES:
        bytes += wParam;
        d->m_bytes->SetLabel(wxString::Format(wxT("%.1fKB"), (float)bytes/1024));
        break;
      case WM_CAN_CHANGE_ACTIVE:
        ::wxLogMessage(_T("Active state: %d"), wParam);
        break;
      case WM_CAN_PORT_EMULATOR:
        ::wxLogMessage(_T("Emulation state: %d"), wParam);
        break;
      case WM_CAN_CHANGE_ENGINE_TEMP:
        ::wxLogMessage(_T("Engine temp: %d"), wParam);
        break;
      case WM_CAN_CHANGE_IN_CAR_TEMP:
        ::wxLogMessage(_T("Car temp: %d"), wParam);
        break;
      case WM_CAN_CHANGE_GEARBOX_TEMP:
        ::wxLogMessage(_T("Gearbox temp: %d"), wParam);
        break;

      case UWM_NFORE_BT_CMD_ERROR:
        ::wxLogMessage(_T("BT: fail cmd 0x%02x err_code 0x%02x"), wParam, lParam);
        break;
      case UWM_NFORE_BT_RUNNING_STATE:
        ::wxLogMessage(_T("BT: running %d"), wParam);
        break;
      case UWM_NFORE_BT_RESET_DONE: {
        BT_NFORE_Init();
        ::wxLogMessage(_T("BT: module reset done, reiniting"));
      } break;
      case UWM_NFORE_BT_PAIR_DEVICE_LIST: {
        LPBT_DEVICE dev = (LPBT_DEVICE)lParam;
        WCHAR mac[30] = {0};
        wsprintf(mac, L"{%02x:%02x:%02x:%02x:%02x:%02x}", dev->MAC[0], dev->MAC[1], dev->MAC[2], dev->MAC[3], dev->MAC[4], dev->MAC[5]);
        ::wxLogMessage(wxT("BT: pair list %s"), mac);
        d->fr->g_devices[dev->index] = dev;
        d->fr->UpdateList();
      } break;
      case UWM_NFORE_BT_PAIR_DEVICE_DELETE: {
        ::wxLogMessage(_T("BT: paired delete %d #%d"), wParam, lParam);
        d->fr->g_devices[wParam] = NULL;
        d->fr->UpdateList();
      } break;
      case UWM_NFORE_BT_PAIR_DEVICE_LIST_EMPTY:
        ::wxLogMessage(_T("BT: paired list empty"));
        break;
      case UWM_NFORE_BT_PAIR_DEVICE_LIST_END:
        ::wxLogMessage(_T("BT: paired list end"));
        break;
      case UWM_NFORE_BT_DEVICE_FOUND: {
        LPBT_DEVICE dev = (LPBT_DEVICE)lParam;
        WCHAR name[255] = {0}, mac[30] = {0};
        wsprintf(mac, L"{%02x:%02x:%02x:%02x:%02x:%02x}", dev->MAC[0], dev->MAC[1], dev->MAC[2], dev->MAC[3], dev->MAC[4], dev->MAC[5]);
        UINT64 umac = 
          UINT64(dev->MAC[0]) << 0x00 |
          UINT64(dev->MAC[1]) << 0x08 |
          UINT64(dev->MAC[2]) << 0x10 |
          UINT64(dev->MAC[3]) << 0x18 |
          UINT64(dev->MAC[4]) << 0x20 |
          UINT64(dev->MAC[5]) << 0x28;
        ::wxLogMessage(_T("BT: dev found %s, %I64u"), mac, umac);
        d->fr->g_devices[dev->index+5] = dev;
        d->fr->UpdateList();
      } break;
      case UWM_NFORE_BT_DEVICE_INFO: {
        LPBT_DEVICE dev = (LPBT_DEVICE)lParam;
        WCHAR mac[30];
        wsprintf(mac, L"{%02x:%02x:%02x:%02x:%02x:%02x}", dev->MAC[0], dev->MAC[1], dev->MAC[2], dev->MAC[3], dev->MAC[4], dev->MAC[5]);
        ::wxLogMessage(_T("BT: dev info %s %s"), ::wxString(::wxConvCurrent->cMB2WC(dev->name.data, dev->name.length, 0)), mac);
        d->fr->UpdateList();
      }
      case UWM_NFORE_BT_SEARCH_DEVICES_END: {
        ::wxLogMessage(_T("BT: search end"));
      } break;
      case UWM_NFORE_BT_PAIR_DEVICE_CONNECT: {
        ::wxLogMessage(_T("BT: connected %d"), wParam);
      } break;
      case UWM_NFORE_BT_PAIR_DEVICE: {
        ::wxLogMessage(_T("BT: paired %d"), lParam);
      } break;
      case UWM_NFORE_BT_READ_REMOTE_INFO: {
        ::wxLogMessage(_T("BT: remote %d"), wParam);
        d->fr->UpdateList();
      } break;
      default: {
        if (msg > WM_USER && msg < WM_USER + 100)
        ::wxLogMessage(_T("BT: msg %d"), msg-WM_USER);
      }
    }

    return 0;
  }


  int Dumper::OnExit () {
    ::DestroyWindow(this->m_HWND);
    ::CAN_Close();
    ::BT_NFORE_Close();
    return 0;//::wxApp::OnExit();
  }