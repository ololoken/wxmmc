#pragma once

#include <wx/wx.h>
#include "MainFrame.h"
#include "bt_nfore.h"

  class Dumper : public wxApp {
    protected:
      wxStaticText* m_bytes;
      HWND m_HWND;
      bool m_logging;
      bool m_nmeaMapping;
      
      wxButton* m_bLogToggle;
      wxButton* m_bNMEAToggle;

    public:
      bool OnInit ();
      int OnExit ();
      
      static LRESULT CALLBACK Dumper::Receiver (HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
      
      void LogToggle ();
      void NMEAToggle ();
      
      ::MainFrame *fr;
      
    
    private:
      
      
      
  };
  
  DECLARE_APP(Dumper)
