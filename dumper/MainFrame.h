#pragma once
#include <wx/wx.h>
#include <wx/grid.h>
#include "bt_nfore.h"

class MainFrame : public wxFrame {
  public:
    MainFrame (void);
    ~MainFrame (void);
    
    void OnExit (wxCommandEvent &e);
    void OnLogToggle (wxCommandEvent &e);
    void OnNMEAToggle (wxCommandEvent &e);
    
    static void SysInfoUpdate (LPVOID lpParam);
    
    ::wxGrid* grid;
    int m_sellRow;
    LPBT_DEVICE g_devices[12];
    BT_DEVICE renderList[20];
    bool m_searching;
    
    
    void OnBTReset (::wxCommandEvent &e);
    void OnBTInit (::wxCommandEvent &e);
    void OnBTPin (::wxCommandEvent &e);
    void OnBTSPP (::wxCommandEvent &e);
    void OnBTVisible (::wxCommandEvent &e);
    void OnBTSearch (::wxCommandEvent &e);
    void OnBTLineTest (::wxCommandEvent &e);
    void OnBTPairList (::wxCommandEvent &e);
    void OnBTConnect (::wxCommandEvent &e);
    void OnBTPair (::wxCommandEvent &e);
    void OnBT3G (::wxCommandEvent &e);
    
    void OnChangeRow (::wxGridEvent &e);
    
    void UpdateList ();
   
    
    DECLARE_EVENT_TABLE()
};
