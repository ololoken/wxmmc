#include "MainFrame.h"
#include "dumper.h"
#include <iostream>
#include <exception>
#include <tapi.h>

  BEGIN_EVENT_TABLE(MainFrame, ::wxFrame)
    EVT_BUTTON(wxID_EXIT, MainFrame::OnExit)
    EVT_BUTTON(wxID_OPEN, MainFrame::OnLogToggle)
    EVT_BUTTON(wxID_CLOSE, MainFrame::OnNMEAToggle)
  END_EVENT_TABLE()


MainFrame::MainFrame (void) : wxFrame(NULL, wxID_ANY, _("Dumper"), wxPoint(0, 0), wxSize(800, 480)) {
  this->ShowFullScreen(true);
  
  this->grid = new ::wxGrid(
    new ::wxWindow(this, wxID_ANY, ::wxPoint(500, 0), ::wxSize(300, 400)), 
    wxID_ANY, ::wxPoint(0, 0), ::wxSize(300, 400));
  this->grid->CreateGrid(12, 2, wxGrid::wxGridSelectRows);
  this->grid->SetColSize(0, 150);
  this->grid->SetColSize(1, 150);
  this->grid->SetRowLabelSize(0);
  this->grid->SetColLabelSize(0);
  
  memset(&this->g_devices[0], 0x00, sizeof(this->g_devices)*sizeof(LPBT_DEVICE));
  
  for (int i = 0; i < 12; i++) {
    for (int j = 0; j < 2; j++) {
      this->grid->SetReadOnly(i, j);
      this->grid->SetCellValue(i, j, wxT("--"));
      this->grid->SetCellRenderer(i, j, new ::wxGridCellStringRenderer());
    }
  }
  
  this->m_searching = false;
  
  this->Connect(
    (new wxButton(this, wxID_ANY, wxT("BT_RESET"), wxPoint(0, 300), wxSize(100, 30)))->GetId(), 
    ::wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(MainFrame::OnBTReset)
  );

  this->Connect(
    (new wxButton(this, wxID_ANY, wxT("BT_VISIBLE"), wxPoint(100, 300), wxSize(100, 30)))->GetId(),
    ::wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(MainFrame::OnBTVisible)
  );
    
  this->Connect(
    (new wxButton(this, wxID_ANY, wxT("BT_SEARCH"), wxPoint(200, 300), wxSize(100, 30)))->GetId(),
    ::wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(MainFrame::OnBTSearch)
  );
  
  this->Connect(
    (new wxButton(this, wxID_ANY, wxT("BT_3G"), wxPoint(300, 300), wxSize(100, 30)))->GetId(),
    ::wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(MainFrame::OnBT3G)
  );

  this->Connect(
    (new wxButton(this, wxID_ANY, wxT("BT_PLIST"), wxPoint(0, 340), wxSize(100, 30)))->GetId(),
    ::wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(MainFrame::OnBTPairList)
  );
  
  this->Connect(
    (new wxButton(this, wxID_ANY, wxT("BT_CONNECT"), wxPoint(100, 340), wxSize(100, 30)))->GetId(),
    ::wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(MainFrame::OnBTConnect)
  );

  this->Connect(
    (new wxButton(this, wxID_ANY, wxT("BT_PAIR"), wxPoint(200, 340), wxSize(100, 30)))->GetId(),
    ::wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(MainFrame::OnBTPair)
  );
  
  this->Connect(
    (new wxButton(this, wxID_ANY, wxT("BT_INIT"), wxPoint(000, 380), wxSize(100, 30)))->GetId(),
    ::wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(MainFrame::OnBTInit)
  );
  
  this->Connect(
    (new wxButton(this, wxID_ANY, wxT("BT_PIN"), wxPoint(100, 380), wxSize(100, 30)))->GetId(),
    ::wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(MainFrame::OnBTPin)
  );
  
  this->Connect(
    (new wxButton(this, wxID_ANY, wxT("BT_SPP"), wxPoint(200, 380), wxSize(100, 30)))->GetId(),
    ::wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(MainFrame::OnBTSPP)
  );
  
  this->Connect(
    (new wxButton(this, wxID_ANY, wxT("BT_LINE"), wxPoint(300, 380), wxSize(100, 30)))->GetId(),
    ::wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(MainFrame::OnBTLineTest)
  );

}

void MainFrame::SysInfoUpdate (LPVOID lpParam) {
    static DWORD
    startTick = ::GetTickCount(),
    startIdle = ::GetIdleTime();
  MEMORYSTATUS memStatus;
  memStatus.dwLength = sizeof(memStatus);
  ::GlobalMemoryStatus(&memStatus);

  ((wxStaticText*)lpParam)->SetLabel(wxString::Format(wxT("%d%/%d%"),
    memStatus.dwMemoryLoad, 
    100-100*(::GetIdleTime()-startIdle) / (::GetTickCount()-startTick)
  ));
  
  startTick = ::GetTickCount();
  startIdle = ::GetIdleTime();
}

void MainFrame::OnExit (wxCommandEvent &e) {
  ::wxExit();
}

void MainFrame::OnLogToggle (wxCommandEvent &e) {
  wxGetApp().LogToggle();
}

void MainFrame::OnNMEAToggle (wxCommandEvent &e) {
  wxGetApp().NMEAToggle();
}


void MainFrame::OnBTReset (::wxCommandEvent &e) {
  wxLogMessage(wxT("BT: Reset: %d"), ::BT_NFORE_Reset());
}

void MainFrame::OnBTInit (::wxCommandEvent &e) {
  wxLogMessage(wxT("BT: Init: %d"), ::BT_NFORE_Init());
}

void MainFrame::OnBTPin (::wxCommandEvent &e) {
  BT_PIN pin = {0};
  pin.data[0] = '0';
  pin.data[1] = '0';
  pin.data[2] = '0';
  pin.data[3] = '0';
  pin.length = 4;
  BT_NFORE_SetPin(&pin);
}

void MainFrame::OnBTVisible (::wxCommandEvent &e) {
  BT_NFORE_SetVisible(::BT_VIS_ALL);
}

void MainFrame::OnBTSearch (::wxCommandEvent &e) {
  if (!this->m_searching) {
    m_searching = true;
    ::BT_NFORE_DevSearchStart((LPBT_INQUERY_CoD)&::NFORE_BT_INQUERY_NETWORK);
  }
  else {
    m_searching = false;
    ::BT_NFORE_DevSearchStop();
  }
  ::wxLogMessage(m_searching ? wxT("BT: Search started") : wxT("BT: Search stopping"));
}

VOID FAR PASCAL lineCallbackFunc( 
  DWORD hDevice, 
  DWORD dwMsg, 
  DWORD dwCallbackInstance, 
  DWORD dwParam1, 
  DWORD dwParam2, 
  DWORD dwParam3
) {
  wxLogMessage(wxT("%d - %d | %d | %d"), dwMsg, dwParam1, dwParam2, dwParam3);
}

void MainFrame::OnBTLineTest (::wxCommandEvent &e) {
  HLINEAPP hLApp;
  HLINE hLine;
  DWORD dwNumDev = 0;
  if (::lineInitialize(&hLApp, ::wxGetInstance(), &lineCallbackFunc, L"dumper", &dwNumDev)) {
    ::wxLogMessage(L"LN: initialize fail");
    return;
  }
  DWORD dwApiVersion;
  if (lineNegotiateAPIVersion(hLApp, 0, 0x00010003, TAPI_CURRENT_VERSION, &dwApiVersion, NULL)) {
    ::wxLogMessage(L"LN: version fail");
    return;
  }
  for (DWORD nDev = 0; nDev < dwNumDev; nDev++) {
    if (::lineOpen(hLApp, nDev, &hLine, dwApiVersion, 0, 0, LINECALLPRIVILEGE_MONITOR, LINEMEDIAMODE_DATAMODEM, NULL)) {
      ::wxLogMessage(L"LN: %d dev fail", nDev);
      continue;
    }
    LINEDEVSTATUS linestatus = {0};
    linestatus.dwTotalSize = sizeof(linestatus);
    if (lineGetLineDevStatus(hLine, &linestatus) == 0) {
    //int signal = ((linestatus.dwSignalLevel + 1) * 100) >> 16;
      ::wxLogMessage(L"LN: #%d signal - 0x%x; flag: 0x%x", nDev, linestatus.dwSignalLevel, linestatus.dwDevStatusFlags);
    }
    lineClose(hLine);
    hLine = 0;
  }
  ::lineShutdown(hLApp);
}

void MainFrame::OnBTPairList (::wxCommandEvent &e) {
  e.Skip();
  BT_NFORE_PairList();
  this->UpdateList();
}

void MainFrame::OnBTSPP (::wxCommandEvent &e) {
  ::NFORE_CMD cmd = {0};
  CHAR str[9] = "helloSPP";
  cmd.code = NFORE_BT_CMD_SPP;
  cmd.length = 9;
  memcpy(&cmd.payload[0], &str[0], cmd.length);
  BT_NFORE_SendCmd(&cmd);
}

void MainFrame::OnChangeRow (::wxGridEvent &e) {
  e.Skip();
}

void MainFrame::OnBTConnect (::wxCommandEvent &e) {
  int row = this->grid->GetCursorRow();
  LPBT_DEVICE dev = &renderList[row];
  if (dev && dev->index) {
    ::BT_NFORE_Connect(dev->MAC, dev->index);
  }

}

void MainFrame::OnBTPair (::wxCommandEvent &e) {
  int row = this->grid->GetCursorRow();
  LPBT_DEVICE dev = &renderList[row];
  if (dev && dev->index) {
    ::BT_NFORE_Pair(dev->MAC);
  }
}

void MainFrame::OnBT3G (::wxCommandEvent &e) {
  NFORE_CMD cmd = {0};
  cmd.code = NFORE_BT_CMD_MODE_3G;
  cmd.length = 2;
  cmd.payload[0] = 0x01;
  cmd.payload[1] = 0x01;
  ::BT_NFORE_SendCmd(&cmd);
}


MainFrame::~MainFrame(void) {
}


void MainFrame::UpdateList () {
  LPBT_DEV_LIST 
    found = ::BT_NFORE_GetFound(),
    paired = ::BT_NFORE_GetPaired();
  memset(&renderList, 0x00, sizeof(renderList));
  int j = 0, k = 0;
  for (int i = 0; i < BT_MAX_DEVICES; i++) {
    if ((*paired)[i].index) {
      renderList[j] = (*paired)[i];
      ++j;
    }
  }
  for (int i = 0; i < BT_MAX_DEVICES; i++) {
    if ((*found)[i].index) {
      renderList[j+k] = (*found)[i];
      ++k;
    }
  }
  for (int i = 0; i < 12; i++) {
    BT_DEVICE dev = renderList[i];//*(this->g_devices[i]);
    if (dev.index) {
      if (dev.name.length) {
        this->grid->SetCellValue(i, 0, ::wxString(dev.name.data, ::wxMBConvUTF16BE(), dev.name.length));
      }
      else {
        this->grid->SetCellValue(i, 0, ::wxString::Format(L"{%02x:%02x:%02x:%02x:%02x:%02x}", dev.MAC[0], dev.MAC[1], dev.MAC[2], dev.MAC[3], dev.MAC[4], dev.MAC[5]));
      }
      if (dev.paired) {
        this->grid->SetCellValue(i, 1, wxT("paired"));
      }
      else if (dev.connected) {
        this->grid->SetCellValue(i, 1, wxT("connected"));
      }
      else {
        this->grid->SetCellValue(i, 1, wxT("--"));
      }
    }
    else {
      this->grid->SetCellValue(i, 0, wxT("--"));
      this->grid->SetCellValue(i, 1, wxT("--"));
    }
  }
}