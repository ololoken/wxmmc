#pragma once
#include <windows.h>

#if defined(MMC_KALINA)
#pragma comment(lib, "MMC23.lib")
#elif defined(MMC_GRANTA)
#pragma comment(lib, "MMC21.lib")
#else 
#error MMC_KALINA or MMC_GRANT not defined
#endif

//possible binary mask
#define MMC_STATE_RADIO   0
#define MMC_STATE_MEDIA   1
#define MMC_STATE_BT      2
#define MMC_STATE_BTMEDIA 3
#define MMC_STATE_BTRADIO 4

#define MMC_RADIO_BAND_FM_OIRT 0//valid values 6500-7400 (OIRT)
#define MMC_RADIO_BAND_FM_CCIR 1//valid values 8750-10800
#define MMC_RADIO_BAND_MW      2//valid values 522-1620 kHz medium vawe freqs

#define MMC_RADIO_SEARCH_LOCAL 0
#define MMC_RADIO_SEARCH_DX    1

#pragma region messages
static const WCHAR * const MMC_MESSAGES[] = {
  L"UWM_RADIO_AUTOSEARCH_END-{7049499B-0480-44f9-BF71-4AE91FA0443C}",
  L"UWM_RADIO_AMS_END-{7049499B-0480-44f9-BF71-4AE91FA0443C}",
  L"UWM_RADIO_TUNED_OK-{7049499B-0480-44f9-BF71-4AE91FA0443C}",
  L"UWM_RADIO_NEW_FREQ-{7049499B-0480-44f9-BF71-4AE91FA0443C}",
  L"UWM_RADIO_BAND-{7049499B-0480-44f9-BF71-4AE91FA0443C}",
  L"UWM_RADIO_STEREO_UPDATE-{7049499B-0480-44f9-BF71-4AE91FA0443C}",
  L"UWM_RADIO_LODX_MOD-{7049499B-0480-44f9-BF71-4AE91FA0443C}",
  L"UWM_RADIO_PTY_SEARCH_END-{7049499B-0480-44f9-BF71-4AE91FA0443C}",
  L"UWM_RADIO_NEW_PS-{7049499B-0480-44f9-BF71-4AE91FA0443C}",
  L"UWM_RADIO_NEW_PTY-{7049499B-0480-44f9-BF71-4AE91FA0443C}",
  L"UWM_RADIO_TP_UPDATE-{7049499B-0480-44f9-BF71-4AE91FA0443C}",
  L"UWM_RADIO_TA_UPDATE-{7049499B-0480-44f9-BF71-4AE91FA0443C}",
  L"UWM_RADIO_RT_UPDATE-{7049499B-0480-44f9-BF71-4AE91FA0443C}",
  L"UWM_RADIO_AF_UPDATE-{7049499B-0480-44f9-BF71-4AE91FA0443C}",
  L"UWM_RADIO_EON_UPDATE-{7049499B-0480-44f9-BF71-4AE91FA0443C}",
  L"UWM_RADIO_PREFREQ_UPDATE-{7049499B-0480-44f9-BF71-4AE91FA0443C}",
  L"UWM_RADIO_LOC_SEN-{7049499B-0480-44f9-BF71-4AE91FA0443C}",
  L"UWM_RADIO_IC_MSG-{7049499B-0480-44f9-BF71-4AE91FA0443C}",
  L"UWM_RADIO_AF_FREQ-{7049499B-0480-44f9-BF71-4AE91FA0443C}",
  L"UWM_BT_DIAL_START-{AE47AD94-3790-40b7-A0D2-AF478F233104}",
  L"UWM_BT_NEW_CALL-{AE47AD94-3790-40b7-A0D2-AF478F233104}",
  L"UWM_AUD_VOL-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_AUD_MUTE-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_AUD_SOUND_READY-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UMW_AUD_BASS-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_AUD_MID-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_AUD_TREBLE-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_AUD_BALANCE-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_AUD_FADER-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_AUD_LOUD-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_AUD_EQ-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_AUD_MIXVOL-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_AUD_SUBWOOF-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_AUD_MIXACT-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_AUD_LOUDC-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UMW_AUD_BASSC-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_AUD_MIDC-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_AUD_TREBLEC-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UMW_AUD_BASSQ-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_AUD_MIDQ-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_COMAND-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_KEY_MODE-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_KEY_PUSHENC-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_KEY-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_KEY_MUTE-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UMW_KEY_VOL-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_KEY_RADIO-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_KEY_MEDIA-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_KEY_NAVI-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_KEY_PREV-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_KEY_NEXT-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_KEY_BT_ANS-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_KEY_BT_CANCEL-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_KEY_ENTER-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_KEY_ASPS-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_KEY_INFO-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_KEY_MENU-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_KEY_AUDIO-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_KEY_AF-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_SYS_POWER_UP-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_SYS_POWER_DOWN-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_SYS_INIT_END-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_SYS_USB-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_SYS_EEPROM-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_RDS_TIME_UPDATA-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_MCU_TIME_UPDATA-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_PAGE_UPDATA-{20E33C37-C776-40ad-9AB0-D80BD031DB13}", 
  L"UWM_VID_CONTRAST-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_VID_BRIGHT-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_VID_COLOR-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_VID_LIGHTMODE-{20E33C37-C776-40ad-9AB0-D80BD031DB13}",
  L"UWM_VID_BACKLIGHT-{20E33C37-C776-40ad-9AB0-D80BD031DB13}"
};
#pragma endregion messages

typedef struct MMCKey {
  enum {
    Mute = 0,
    Long = 1,
    Down = 2,
    Up = 3,
    Mode = 3,
    Power = 4,
    Radio = 5,//lparam 1 long
    Media = 6,
    Screen = 7,
    Prv = 8,//lparam 3 up; 2 down
    Nxt = 9,//lparam 3 up; 2 down
    BtAnsw = 10,
    BtRedj = 12,
    OK = 11,//lparam 3 up; 2 down
    ASPS = 13,
    Info = 14,
    Menu = 15,
    Audio = 16,
    AF = 17,
    RadioLong = 69,
    MediaLong = 70,
    ScreenLing = 71,
    BtAnswLong = 74,
    BtCancelLing = 76,
    ASPSLong = 77,
    InfoLong = 78,
    MenuLong = 79,
    AudioLong = 80,
    AFLong = 81,
    PrevDown = 136,
    NextDown = 137,
    EncoderDown = 139,
    PrevUp = 200,
    NextUp = 201,
    EncoderUp = 203
  };
} MMCKey;

#define MMC_HW_API __declspec(dllimport)

#ifdef __cplusplus
extern "C" {
#endif
MMC_HW_API DWORD API_CallRadioValue (DWORD idx/*0-22; >22 -> 0*/);//callback in UWM_RADIO_IC_MSG
MMC_HW_API int API_GetCAMState ();
MMC_HW_API int API_GetRadioValue (DWORD idx/*0-22; >22 -> 0*/);
MMC_HW_API BOOL API_GetRaidoSoftmuteHighcutState ();
//MMC_HW_API API_RadioAFFreq
//MMC_HW_API API_RadioStationIfc
//MMC_HW_API API_RadioStationLevel
//MMC_HW_API API_RadioStationUsn
//MMC_HW_API API_RadioStationWam
//MMC_HW_API API_SetAuidoChannel
//MMC_HW_API API_SetCAMState
//MMC_HW_API API_SetGpsAudioTimeMute
//MMC_HW_API API_SetGpsAudioTimeRelease
//MMC_HW_API API_SetRadioValue
//MMC_HW_API API_SetRaidoSoftmuteHighcutState
//MMC_HW_API Api_BtReset
//MMC_HW_API Api_CallEepromData
MMC_HW_API int Api_GetActivateMode ();
MMC_HW_API time_t Api_GetCurrentTime ();
MMC_HW_API char* Api_GetDLLversion ();
//MMC_HW_API Api_GetEepromData
MMC_HW_API HWND Api_GetHwnd ();
MMC_HW_API void* Api_GetMCUversion ();
MMC_HW_API BOOL Api_GetMuteState ();
MMC_HW_API time_t Api_GetOffTime ();
MMC_HW_API time_t Api_GetRDSTime ();
MMC_HW_API DWORD Api_GetSysInitState ();
MMC_HW_API DWORD Api_GetUsbState ();
MMC_HW_API void Api_Reset ();
MMC_HW_API DWORD Api_SetActivateMode (DWORD);
MMC_HW_API BOOL Api_SetBTMuteState (BOOL);
MMC_HW_API BOOL Api_SetCurrentTime (time_t);
//MMC_HW_API Api_SetEepromData
//MMC_HW_API Api_SetEncoderMode
MMC_HW_API BOOL Api_SetHwnd (HWND);
MMC_HW_API BOOL Api_SetMuteState (BOOL);
//MMC_HW_API Api_SetNavAudioState not implemented
//MMC_HW_API Api_SetNavReset not implemented
//MMC_HW_API int Api_SetOffTime (int); not implemented
MMC_HW_API BOOL Api_SetSleepReady (BOOL);
//MMC_HW_API Api_SetTouch
//MMC_HW_API Aud_GetBalance
//MMC_HW_API Aud_GetBass
//MMC_HW_API Aud_GetBassCFre
//MMC_HW_API Aud_GetBassQ
//MMC_HW_API Aud_GetFader
//MMC_HW_API Aud_GetLoud
//MMC_HW_API Aud_GetLoudCFre
//MMC_HW_API Aud_GetMid
//MMC_HW_API Aud_GetMiddleCFre
//MMC_HW_API Aud_GetMiddleQ
//MMC_HW_API Aud_GetMixAct
//MMC_HW_API Aud_GetMixVol
//MMC_HW_API Aud_GetTreble
//MMC_HW_API Aud_GetTrebleCFre
MMC_HW_API int Aud_GetVolume (DWORD);
//MMC_HW_API Aud_SetBalance
//MMC_HW_API Aud_SetBass
//MMC_HW_API Aud_SetBassCFre
//MMC_HW_API Aud_SetBassQ
//MMC_HW_API Aud_SetFader
//MMC_HW_API Aud_SetLoud
//MMC_HW_API Aud_SetLoudCFre
//MMC_HW_API Aud_SetMid
//MMC_HW_API Aud_SetMiddleCFre
//MMC_HW_API Aud_SetMiddleQ
//MMC_HW_API Aud_SetMixAct
//MMC_HW_API Aud_SetMixVol
//MMC_HW_API Aud_SetTreble
//MMC_HW_API Aud_SetTrebleCFre
MMC_HW_API int Aud_SetVolume (DWORD);
//MMC_HW_API Audi_GetSetting
//MMC_HW_API Audi_SetSetting
MMC_HW_API int MMC21_init ();
MMC_HW_API int MMC21_uninit ();
//MMC_HW_API Radio_AMS
MMC_HW_API BOOL Radio_AutoSearch (BOOL down);
MMC_HW_API DWORD Radio_GetCurrentBand ();
MMC_HW_API DWORD Radio_GetCurrentFreq ();//base is 8600.000 = 80.00
//MMC_HW_API Radio_GetPresetFreq
//MMC_HW_API Radio_GetPresetNum
MMC_HW_API int Radio_GetSearchLevel ();
MMC_HW_API LPVOID Radio_GetStereoMsg ();
MMC_HW_API BOOL Radio_GetStereoStatus ();
MMC_HW_API BOOL Radio_ManSearch (BOOL down);
MMC_HW_API int Radio_SCAN();
MMC_HW_API DWORD Radio_SetBand (DWORD);
MMC_HW_API DWORD Radio_SetFreq (DWORD);
MMC_HW_API int Radio_SetSearchLevel (int);
MMC_HW_API BOOL Radio_SetStereoStatus (BOOL);
//MMC_HW_API Radio_StorePreset
MMC_HW_API void* Rds_GetAFStatus ();
//MMC_HW_API Rds_GetEONStatus
MMC_HW_API BYTE* Rds_GetPSdata ();
MMC_HW_API char* Rds_GetPTYMsg ();
MMC_HW_API void* Rds_GetPTYState ();
MMC_HW_API BYTE* Rds_GetRTStatus ();
//MMC_HW_API Rds_GetTAStatus
//MMC_HW_API Rds_GetTAmsg
//MMC_HW_API Rds_GetTPStatus
MMC_HW_API BOOL Rds_SetAFMsg (BOOL);
//MMC_HW_API Rds_SetPTYSwitch
//MMC_HW_API Rds_SetTASwitch
//MMC_HW_API Rds_StartPTYSearch
//MMC_HW_API RegisterFunc
MMC_HW_API int Vid_GetBackLight ();
//MMC_HW_API Vid_GetBackLightMode
//MMC_HW_API Vid_GetBright
//MMC_HW_API Vid_GetColor
//MMC_HW_API Vid_GetLightMode
//MMC_HW_API Vid_SetBackLight
//MMC_HW_API Vid_SetBackLightMode
MMC_HW_API int Vid_SetBright (int);
MMC_HW_API int Vid_SetColor (int);
MMC_HW_API int Vid_SetContrast (int);
MMC_HW_API int Vid_SetLightMode (DWORD);
//MMC_HW_API Vid_SetTouchCalibrate
MMC_HW_API int Vid_getContrast ();
//MMC_HW_API MMC_HW_API int radio_RecallPreset ()
#ifdef __cplusplus
}
#endif
