#include "wxCanEvent.h"
#include "wxMMCApp.h"

DEFINE_EVENT_TYPE(wxCAN_EVENT);

namespace mmc {

  IMPLEMENT_DYNAMIC_CLASS(wxCanEvent, wxEvent);

  wxCanEvent::wxCanEvent() : ::wxEvent(0, wxCAN_EVENT) {

  }

  wxCanEvent::wxCanEvent (int id, int data) : ::wxEvent(id, wxCAN_EVENT) {
    this->m_canData = data;
  }

  wxCanEvent::wxCanEvent (const wxCanEvent &event) : ::wxEvent(event) {
    m_canData = event.m_canData;
  }

  void wxCanEvent::Broadcast () {
    wxGetApp().Broadcast(*this);
  }

  wxCanEvent::~wxCanEvent () {}

}