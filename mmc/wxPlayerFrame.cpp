#include "wxPlayerFrame.h"
#include "wxPlayer.h"

namespace mmc {

  BEGIN_EVENT_TABLE(wxPlayerFrame, wxMMCFrame)

  END_EVENT_TABLE()

  const wxSize wxPlayerFrame::FullScreen = wxSize(800, 480);

  wxPlayerFrame::wxPlayerFrame (::wxWindow* parent) : wxMMCFrame(parent, L"player") {
    this->m_playerWindow = new ::wxWindow(this, wxID_ANY, wxPoint(100, 0), wxSize(600, 400), wxBORDER_NONE|wxSTAY_ON_TOP);
    this->m_playerWindow->SetBackgroundColour(*::wxBLACK);
    wxLogMessage(wxT("pl init %d"), wxPlayer::Get().Init(this->m_playerWindow));
    wxLogMessage(wxT("pw: %d"), this->m_bgImgLoaded);
    
    this->Connect(
      (new ::wxButton(this, wxID_ANY, _T(":exit"), wxPoint(0, 0)))->GetId(), ::wxEVT_COMMAND_BUTTON_CLICKED, 
      wxCommandEventHandler(wxPlayerFrame::OnExit)
    );
    this->Connect(
      (new ::wxButton(this, wxID_ANY, _T(":open"), wxPoint(0, 40)))->GetId(), ::wxEVT_COMMAND_BUTTON_CLICKED, 
      wxCommandEventHandler(wxPlayerFrame::OnOpen)
    );
  }

  wxPlayerFrame::~wxPlayerFrame () {
    wxPlayer::Get().DeInit();
  }
  
  wxWindow* wxPlayerFrame::GetPlayerWindow () {
    return this->m_playerWindow;
  }
  
  void wxPlayerFrame::OnExit (wxCommandEvent &e) {
    this->Destroy();
  }
  
  void wxPlayerFrame::OnOpen (wxCommandEvent &e) {
    wxPlayer::Get().Play(::wxString(L"\\StaticStore\\S01E01.avi"));
  }

}