#include "wxBtHandler.h"
#include <wx/stdpaths.h>
#include <wx/filename.h>
#include "constants.h"

namespace mmc {

  wxBtHandler::wxBtHandler () : ::wxThread(::wxTHREAD_DETACHED) {
    this->m_HWND = ::CreateWindowEx(0, L"Static", L"BtHandlerMessageWindow", 0, 0, 0, 0, 0, 0, NULL, ::wxGetInstance(), NULL);
    ::SetWindowLong(this->m_HWND, GWL_WNDPROC, (long)&wxBtHandler::Receiver);
    ::SetWindowLong(this->m_HWND, GWL_USERDATA, (long)this);
    this->Create();
  }

  wxThread::ExitCode wxBtHandler::Entry() {
    ::BT_NFORE_Open(this->m_HWND);
    while (!this->TestDestroy()) {
      this->Sleep(50);
    }
    ::BT_NFORE_Close();
    return (wxThread::ExitCode)0;
  }

  LRESULT CALLBACK wxBtHandler::Receiver (HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
    wxBtHandler* h = (wxBtHandler*)GetWindowLong(hwnd, GWL_USERDATA);
    if (msg > WM_USER) {
      wxBtEvent(msg, wParam, lParam).Broadcast();
    }
    return 0;
  }

  wxBtHandler::~wxBtHandler () {
    ::DestroyWindow(this->m_HWND);
  }

}