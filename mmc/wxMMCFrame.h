#pragma once
#include <wx/wx.h>
#include "wxApiEvent.h"
#include "wxBtEvent.h"
#include "wxCanEvent.h"
#include "wxApiEvent.h"

namespace mmc {

  class wxMMCFrame : public ::wxFrame {
    public:
      static const wxSize FullScreen;
      wxMMCFrame (::wxWindow* parent, const wxString &title, const wxPoint &pos = wxPoint(0, 0), const wxSize &size = FullScreen, long style = wxBORDER_NONE);
      ~wxMMCFrame ();
      virtual void SetBackground (::wxColor color);
      virtual void SetBackground (::wxBitmap bitmap);
      virtual void DrawBackground (::wxDC *dc, const ::wxRect &rect);

    protected:
      wxBitmap m_bgImg;
      wxColor m_bgColor;
      bool m_bgImgLoaded;

    protected:
      virtual void OnExit (wxCommandEvent &e) = 0;
      virtual void OnPaint (wxPaintEvent &e);
      virtual void RelayEvent (::wxEvent &e);

      DECLARE_EVENT_TABLE()
  };

}
