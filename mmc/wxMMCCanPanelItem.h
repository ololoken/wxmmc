#pragma once

#include <wx/wx.h>

namespace mmc {

  class wxMMCCanPanelItem: public wxPanel {
    public:
      static const ::wxSize DefaultSize;
      wxMMCCanPanelItem (wxWindow* parent, const wxBitmap& bm, const wxString& label = wxT(""), const wxPoint &pos = wxDefaultPosition, const wxSize &size = DefaultSize);
      
    public:
      void SetItemIcon (const wxBitmap& bm);
      virtual void SetItemValue (const wxString& label);
      virtual ::wxStaticText* GetLabel ();
      void SetItemLabelOffset (::wxPoint);
      
    protected:
      ::wxStaticText *m_label;
      ::wxStaticBitmap *m_icon;
      ::wxPoint m_labelOffset;
      ::wxString m_labelFormat;
      
    protected:
      ::wxPoint GetLabelPos ();
  };

}