#pragma once
#include <wx/wx.h>
#include "wxMMCFrame.h"

namespace mmc {

  class wxPlayerFrame : public wxMMCFrame {
    public:
      static const wxSize FullScreen;
      wxPlayerFrame (::wxWindow *parent);
      ~wxPlayerFrame ();
      
    public:
      wxWindow* GetPlayerWindow ();

    protected:
      wxWindow *m_playerWindow;

    protected:
      void OnExit (wxCommandEvent &e);
      void OnOpen (wxCommandEvent &e);

      DECLARE_EVENT_TABLE()
  };

}
