#pragma once
#include <wx/wx.h>
#include "wxMMCCanPanelItem.h"
#include "wxMMCFrame.h"
#include "wxCanHandler.h"

namespace mmc {

  WX_DECLARE_LIST(wxMMCCanPanelItem, CanPanelItemsList);

  class wxMMCCanPanel : public wxMMCFrame {
    public:
      wxMMCCanPanel ();
      ~wxMMCCanPanel ();

    protected:
      void OnExit (wxCommandEvent &e) {}
      void Active (wxCanEvent &e);
      void Update (wxCanEvent &e);
      void UpdateEngineTemp (wxCanEvent &e);
      void UpdateGearboxTemp (wxCanEvent &e);
      void UpdateInCarTemp (wxCanEvent &e);
      void UpdateOutCarTemp (wxCanEvent &e);
      void UpdateFuel (wxCanEvent &e);
      void UpdateSpeed (wxCanEvent &e);
      void UpdateFuelInstantConsumption (wxCanEvent &e);
      void UpdateDistance (wxCanEvent &e);
      
      int ItemIndex (const ::wxString& itemName);
      void UpdateItemLabel (const ::wxString& itemName, const ::wxString& label);
      
      virtual void RelayEvent (::wxEvent &e);

    protected:
      static const ::wxString AvailableItems[];

      CanPanelItemsList m_cpItems;

    private:
      DECLARE_EVENT_TABLE()
    
  };

}
