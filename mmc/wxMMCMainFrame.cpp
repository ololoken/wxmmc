#include "wxMMCResource.h"
#include "wxMMCMainFrame.h"
#include "wxMMCButton.h"
#include "wxMMCRadioFrame.h"
#include "wxPlayerFrame.h"
#include "wxMMCApp.h"
#include "constants.h"
#include "pm.h"
#include "wxApiHandler.h"

namespace mmc {

  BEGIN_EVENT_TABLE(wxMMCMainFrame, wxMMCFrame)
    EVT_TIMER(TIMER_SYSINFO_UPDATE, wxMMCMainFrame::OnSysInfoUpdate)
  END_EVENT_TABLE()


  void wxMMCMainFrame::OnExit (wxCommandEvent &e) {
    if (wxYES == wxMessageBox(L":exit", L":confirm", wxYES_NO, this)) {
      ::wxExit();//closing top level windows doesn't fire close event for application on wince, so lets do in explicit way
    }
  }

  wxMMCMainFrame::wxMMCMainFrame () : wxMMCFrame(NULL, L"main") {
    ::ShowWindow(FindWindowW(_T("HHTaskBar"), NULL), SW_HIDE);
    this->m_log = new wxTextCtrl(this, wxID_ANY, wxT("Log...\n"), wxPoint(0, 50), wxSize(400, 250), 
      wxTE_MULTILINE|wxTE_LEFT|wxTE_READONLY
    );
    ::wxLog::SetActiveTarget(new wxLogTextCtrl(this->m_log));

    this->m_naviPrc = new wxNavi();

    this->Connect(
      wxGetApp().Skin->GetButton(this, L"exit")->GetId(),
      ::wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(wxMMCMainFrame::OnExit)
    );
    this->Connect(
      wxGetApp().Skin->GetButton(this, L"radio")->GetId(),
      ::wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(wxMMCMainFrame::OnRadio)
    );
    
    this->Connect(
      wxGetApp().Skin->GetButton(this, L"media")->GetId(),
      ::wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(wxMMCMainFrame::OnMedia)
    );
    this->Connect(
      wxGetApp().Skin->GetButton(this, L"navi")->GetId(),
      ::wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(wxMMCMainFrame::OnNavi)
    );

    this->Connect(
      wxGetApp().Skin->GetButton(this, L"console")->GetId(),
      ::wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(wxMMCMainFrame::OnConsole)
    );
    this->Connect(
      wxGetApp().Skin->GetButton(this, wxT("min"))->GetId(),
      ::wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(wxMMCMainFrame::OnMinimize)
    );
    
    
    this->Connect(
      (new ::wxButton(this, wxID_ANY, L"Test0", wxPoint(500, 400)))->GetId(),
      ::wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(wxMMCMainFrame::OnTest0Action)
    );
    /*
    this->Connect(
      (new ::wxButton(this, wxID_ANY, wxT("Test1"), wxPoint(500, 300)))->GetId(),
      ::wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(wxMMCMainFrame::OnTest1Action)
    ),*/

    this->m_log->Hide();
    this->m_log->Raise();
    
    this->m_sysInfoLabel = new ::wxStaticText(this, wxID_ANY, wxT("--%/--%"), ::wxPoint(10, 230));
    (new ::wxTimer(this, TIMER_SYSINFO_UPDATE))->Start(2000);

  }

  void wxMMCMainFrame::OnSysInfoUpdate (::wxTimerEvent &e) {
    static DWORD
      startTick = ::GetTickCount(),
      startIdle = ::GetIdleTime();
    MEMORYSTATUS memStatus;
    memStatus.dwLength = sizeof(memStatus);
    ::GlobalMemoryStatus(&memStatus);

    this->m_sysInfoLabel->SetLabel(wxString::Format(wxT("%d%/%d%"),
      memStatus.dwMemoryLoad, 
      100-100*(::GetIdleTime()-startIdle) / (::GetTickCount()-startTick)
    ));
    
    startTick = ::GetTickCount();
    startIdle = ::GetIdleTime();
  }

  wxMMCMainFrame::~wxMMCMainFrame () {
    ::ShowWindow(FindWindowW(_T("HHTaskBar"), NULL), SW_SHOW);
  }
  
  void wxMMCMainFrame::OnMedia (wxCommandEvent &e) {
    wxGetApp().ShowFrame(L"player");
  }
  

  void wxMMCMainFrame::OnRadio (wxCommandEvent &e) {
    wxGetApp().ShowFrame(L"radio");
  }
  
  void wxMMCMainFrame::OnNavi (wxCommandEvent &e) {
    this->m_naviPrc->Run(wxGetApp().Cfg->GetNaviPath(), wxGetApp().Cfg->GetNaviName());
  }

  void wxMMCMainFrame::OnConsole (wxCommandEvent &e) {
    this->m_log->Show(!this->m_log->IsShown());
  }
  
  void wxMMCMainFrame::OnMinimize (::wxCommandEvent &e) {

  }

  void wxMMCMainFrame::OnTest0Action (wxCommandEvent &e) {
    wxApiEvent(wxApiHandler::UWM_KEY, ::MMCKey::Nxt, ::MMCKey::Down).Broadcast();
    
  }
  
  void wxMMCMainFrame::OnTest1Action (wxCommandEvent &e) {
    
  }

}