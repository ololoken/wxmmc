#pragma once
#include <wx/wx.h>
#include <wx/jsonreader.h>
#include <wx/dynarray.h>
#include "wxApiEvent.h"
#include "mmc_api.h"

namespace mmc {

  class wxMMCRadioStation {
    public:
      typedef enum RadioBand {
        BAND = -1,
        FM_OIRT = MMC_RADIO_BAND_FM_OIRT,
        FM_CCIR = MMC_RADIO_BAND_FM_CCIR,
        MW = MMC_RADIO_BAND_MW
      } RadioBand;
    
      wxMMCRadioStation (RadioBand band, DWORD fq, const ::wxString& title, const ::wxString& img) {
        this->band = band;
        this->fq = fq;
        this->title = title;
        this->img = img;
      }
      RadioBand band;
      DWORD fq;
      ::wxString title;
      ::wxString img;
  };
  
  WX_DECLARE_OBJARRAY(wxMMCRadioStation, wxMMCRadioStationArray);

  class wxMMCRadio: public ::wxEvtHandler {
    public:
      wxMMCRadio ();
      ~wxMMCRadio ();
   
   public:
    typedef enum SrchLevel {
      LEVEL = -1,
      Local = MMC_RADIO_SEARCH_LOCAL,
      DX = MMC_RADIO_SEARCH_DX
    } SrchLevel;
    typedef enum SrchMode {
      Man = 0,
      Auto = 1
    } SrchMode;
    
  protected:
    SrchMode m_searchMode;
    ::wxJSONValue m_config;
    wxMMCRadioStationArray m_stations;
    int m_currentStation;
    ::wxTimer *m_pressTimer;
    bool m_longPress;
    
    void OnLongPress (::wxTimerEvent &e);

  public:
    wxMMCRadioStation::RadioBand Band (wxMMCRadioStation::RadioBand b = wxMMCRadioStation::BAND);
    DWORD Freq (DWORD f = 0);
    SrchLevel SearchLevel (SrchLevel l = LEVEL);
    bool IsStereo ();
    void Run ();
    void Stop ();
    void Search (bool down, SrchMode mode = Auto);
    void KeyListener (wxApiEvent &e);
    void Next (wxApiEvent &e = wxApiEvent(-1, 0, 0));
    void Prev (wxApiEvent &e = wxApiEvent(-1, 0, 0));
    const wxMMCRadioStationArray& GetStations ();
    int Station (int s = -1);

    private:
      DECLARE_EVENT_TABLE()
  };

}