#pragma once

#include <wx/wx.h>

DECLARE_EVENT_TYPE(wxBT_EVENT, wxID_ANY);
#define EVT_MMCBT(id, fn) \
  DECLARE_EVENT_TABLE_ENTRY(wxBT_EVENT, id, wxID_ANY, (wxObjectEventFunction)(wxEventFunction)&fn, (wxObject*)NULL),

namespace mmc {

  class wxBtEvent: public ::wxEvent {
    public:
      wxBtEvent ();
      wxBtEvent (const wxBtEvent &event);
      wxBtEvent (int id, WPARAM wParam, LPARAM lParam);
      ~wxBtEvent ();

      virtual wxEvent *Clone() const { 
        return new wxBtEvent(*this); 
      }
      
      WPARAM GetBtWPARAM () {return this->m_btWPARAM;}
      LPARAM GetBtLPARAM () {return this->m_btLPARAM;}
      
      void Broadcast ();
    protected:
      WPARAM m_btWPARAM;
      LPARAM m_btLPARAM;

      DECLARE_DYNAMIC_CLASS(wxBtEvent)
  };

  typedef void (wxEvtHandler::*wxBtEventFunction)(wxBtEvent&);

  #define wxBtEventHandler(func) \
    (wxObjectEventFunction)(wxBtEventFunction)wxStaticCastEvent(wxBtEventFunction, &func)
}