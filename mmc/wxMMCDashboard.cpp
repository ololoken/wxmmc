#include "wxMMCApp.h"
#include "wxMMCDashboard.h"

namespace mmc {

  BEGIN_EVENT_TABLE(wxMMCDashboard, wxMMCFrame)

  END_EVENT_TABLE()

  void wxMMCDashboard::OnExit (wxCommandEvent &e) {
    this->Destroy();
  }

  wxMMCDashboard::wxMMCDashboard () : wxMMCFrame(NULL, wxT("dashboard")) {
    
  }

  wxMMCDashboard::~wxMMCDashboard () {

  }

}