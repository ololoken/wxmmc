#include "wxMMCRadioFrame.h"
#include "wxMMCResource.h"
#include "wxMMCButton.h"
#include "wxMMCApp.h"
#include "wxApiHandler.h"

namespace mmc {

  BEGIN_EVENT_TABLE(wxMMCRadioFrame, wxMMCFrame)
  END_EVENT_TABLE()

  wxMMCRadioFrame::wxMMCRadioFrame (::wxWindow* parent, wxMMCRadio *radio) : wxMMCFrame(parent, wxT("radio")) {
    this->m_radio = radio;
    
    this->m_radio->Run();

    this->Connect(
      wxGetApp().Skin->GetButton(this, L"exit")->GetId(),
      ::wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(wxMMCRadioFrame::OnExit)
    );
    this->Connect(
      wxGetApp().Skin->GetButton(this, L"minimize")->GetId(),
      ::wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(wxMMCRadioFrame::OnMinimize)
    );
    
    this->Connect(
      wxGetApp().Skin->GetButton(this, L"freq-dec")->GetId(),
      ::wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(wxMMCRadioFrame::OnDecFreq)
    );
    this->Connect(
      wxGetApp().Skin->GetButton(this, L"freq-inc")->GetId(),
      ::wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(wxMMCRadioFrame::OnIncFreq)
    );
    
    this->Connect(
      wxGetApp().Skin->GetButton(this, L"stations-next")->GetId(),
      ::wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(wxMMCRadioFrame::OnChangeStationsPage)
    );
    this->Connect(
      wxGetApp().Skin->GetButton(this, L"stations-prev")->GetId(),
      ::wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(wxMMCRadioFrame::OnChangeStationsPage)
    );
    this->Connect(
      wxGetApp().Skin->GetButton(this, L"station-next")->GetId(),
      ::wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(wxMMCRadioFrame::OnStationNext)
    );
    this->Connect(
      wxGetApp().Skin->GetButton(this, L"station-prev")->GetId(),
      ::wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(wxMMCRadioFrame::OnStationPrev)
    );
    
    this->m_btnBand = wxGetApp().Skin->GetButton(this, L"band");
    this->Connect(this->m_btnBand->GetId(), ::wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(wxMMCRadioFrame::OnBandSelect));
    this->DrawBand();
    
    this->m_btnRange = wxGetApp().Skin->GetButton(this, L"range");
    this->Connect(this->m_btnRange->GetId(), ::wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(wxMMCRadioFrame::OnSearchLevelSelect));
    this->DrawRange();
    
    this->m_freqLabel = wxGetApp().Skin->GetLabel(this, L"freq");
    this->Connect(wxApiHandler::UWM_RADIO_NEW_FREQ, ::wxAPI_EVENT, wxApiEventHandler(wxMMCRadioFrame::OnUpdateFreq));
    this->DrawFreq();

    this->m_itemWidth = 100;
    this->m_stationsPanel = wxGetApp().Skin->GetPanel(this, L"stations");
    this->m_perPage = (size_t)floor(this->m_stationsPanel->GetSize().x/this->m_itemWidth),
    this->m_pageCurrent = (size_t)floor((double)this->m_radio->Station()/(double)this->m_perPage);
    this->DrawStations(this->m_pageCurrent);

  }

  wxMMCRadioFrame::~wxMMCRadioFrame () {

  }

  void wxMMCRadioFrame::DrawFreq () {
    DWORD f = this->m_radio->Freq();
    this->m_freqLabel->SetLabel(::wxString::Format(f ? L"%.2f" : L"--.--", ((float)f)/100.0f));
  }
  
  void wxMMCRadioFrame::DrawStations (int page) {
    static const wxMMCRadioStationArray arr = this->m_radio->GetStations();
    ::wxPoint p(0, 0);
    size_t
      gap = (size_t)floor((double)(this->m_stationsPanel->GetSize().x-this->m_perPage*this->m_itemWidth)/(double)(this->m_perPage+1)),
      cPage = (page == -1) 
        ? (size_t)floor((double)this->m_radio->Station()/(double)this->m_perPage)
        : page;
    for (size_t i = cPage*this->m_perPage; i < (cPage+1)*this->m_perPage && i < arr.Count(); i++) {
      wxMMCRadioStation st = arr.Item(i);
      ::wxBitmap bm = wxMMCResource::Get(st.img);
      ::wxWindow *wnd = NULL;
      if (bm.IsSameAs(::wxNullBitmap)) {
        wnd = new ::wxButton(
          this->m_stationsPanel, wxID_ANY, wxString::Format(wxT("%.2f\n%s"), (float)st.fq/100.0f, st.title), p, 
          ::wxSize(this->m_itemWidth, this->m_stationsPanel->GetSize().y), wxNO_BORDER
        );
      }
      else {
        wnd = new ::wxBitmapButton(this->m_stationsPanel, wxID_ANY, bm, p, ::wxDefaultSize, wxNO_BORDER);
      }
      wnd->SetClientData((LPVOID)i);
      this->Connect(
        wnd->GetId(), ::wxEVT_COMMAND_BUTTON_CLICKED, 
        wxCommandEventHandler(wxMMCRadioFrame::OnStationChange)
      );
      p.x += wnd->GetSize().x+gap;
    }
  }
  
  void wxMMCRadioFrame::DrawBand () {
    static const ::wxString bands[3] = {
      L"��� OIRT", L"��� CCIR", L"��"
    };
    this->m_btnBand->SetLabel(bands[this->m_radio->Band()]);
    this->m_btnBand->Refresh();
  }
  
  void wxMMCRadioFrame::DrawRange () {
    static const ::wxString ranges[3] = {
      L"�������", L"���"
    };
    this->m_btnRange->SetLabel(ranges[this->m_radio->SearchLevel()]);
    this->m_btnRange->Refresh();
  }

#pragma region events
  void wxMMCRadioFrame::OnUpdateFreq (wxApiEvent &e) {
    this->DrawFreq();
  }
  
  void wxMMCRadioFrame::OnIncFreq (wxCommandEvent &e) {
    this->m_radio->Freq(this->m_radio->Freq()+5);
  }
  
  void wxMMCRadioFrame::OnDecFreq (wxCommandEvent &e) {
    this->m_radio->Freq(this->m_radio->Freq()-5);
  }
  
  void wxMMCRadioFrame::OnExit (wxCommandEvent &e) {
    //shutdown playback
    this->m_radio->Stop();
    this->OnMinimize(e);
  }
  
  void wxMMCRadioFrame::OnMinimize (wxCommandEvent &e) {
    //just destroy window
    this->m_stationsPanel->Destroy();
    this->Destroy();
  }
  
  void wxMMCRadioFrame::OnStationChange (::wxCommandEvent &e) {
    ::wxWindow *wnd = wxDynamicCast(e.GetEventObject(), ::wxWindow);
    if (NULL == wnd) {return;}
    this->m_radio->Station((size_t)wnd->GetClientData());
  }
  
  void wxMMCRadioFrame::OnStationNext (::wxCommandEvent &e) {
    this->m_radio->Next();
  }
  
  void wxMMCRadioFrame::OnStationPrev (::wxCommandEvent &e) {
    this->m_radio->Prev();
  }
  
  void wxMMCRadioFrame::OnChangeStationsPage (::wxCommandEvent &e) {
    wxMMCButton *btn = wxDynamicCast(e.GetEventObject(), wxMMCButton);
    int pagesTotal = (int)floor((double)this->m_radio->GetStations().Count()/this->m_perPage);
    this->m_pageCurrent += L"stations-next" == btn->GetName()
      ? 1
      : -1;
    if (this->m_pageCurrent < 0) {
      this->m_pageCurrent = pagesTotal;
    }
    else if (this->m_pageCurrent > pagesTotal) {
      this->m_pageCurrent = 0;
    }
    this->m_stationsPanel->Destroy();
    this->m_stationsPanel = wxGetApp().Skin->GetPanel(this, L"stations");
    this->DrawStations(this->m_pageCurrent);
  }
  
  void wxMMCRadioFrame::OnBandSelect (::wxCommandEvent &e) {
    DWORD nb = this->m_radio->Band()+1;
    if (nb > 2) {nb = 0;}
    this->m_radio->Band((wxMMCRadioStation::RadioBand)nb);
    this->DrawBand();
  }
  
  void wxMMCRadioFrame::OnSearchLevelSelect (::wxCommandEvent &e) {
    DWORD nr = this->m_radio->SearchLevel()+1;
    if (nr > 1) {nr = 0;}
    this->m_radio->SearchLevel((wxMMCRadio::SrchLevel)nr);
    this->DrawRange();
  }
#pragma endregion events
}