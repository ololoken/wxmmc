#include "wxBtEvent.h"
#include "wxMMCApp.h"

DEFINE_EVENT_TYPE(wxBT_EVENT);

namespace mmc {

  IMPLEMENT_DYNAMIC_CLASS(wxBtEvent, wxEvent);

  wxBtEvent::wxBtEvent() : ::wxEvent(0, wxBT_EVENT) {

  }

  wxBtEvent::wxBtEvent (int id, WPARAM wParam, LPARAM lParam) : ::wxEvent(id, wxBT_EVENT) {
    this->m_btWPARAM = wParam;
    this->m_btLPARAM = lParam;
  }

  wxBtEvent::wxBtEvent (const wxBtEvent &event) : ::wxEvent(event) {
    this->m_btWPARAM = event.m_btWPARAM;
    this->m_btLPARAM = event.m_btLPARAM;
  }

  void wxBtEvent::Broadcast () {
    wxGetApp().Broadcast(*this);
  }

  wxBtEvent::~wxBtEvent () {}

}