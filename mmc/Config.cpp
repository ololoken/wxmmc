#include "Config.h"
#include <wx/stdpaths.h>
#include <wx/filename.h>
#include <wx/wfstream.h>

namespace mmc {
  Config::Config (const wxString& path) : ::wxObject() {
    ::wxFileInputStream configJSON(::wxFileName(::wxStandardPaths::Get().GetExecutablePath()).GetPath()+wxT("/../")+path);
    ::wxJSONReader reader;
    if (0 == reader.Parse(configJSON, &this->root)) {
      
    }
    else {
      ::wxArrayString errs = reader.GetErrors();
      for (size_t i = 0; i < errs.Count(); i++) {
        ::wxLogFatalError(wxT("Config err: %s"), errs[i]);
      }
    }
  }
  
  ::wxJSONValue Config::Get (const ::wxString &name) {
    return this->root[name];
  }
  
  ::wxString Config::GetNaviPath () {
    int selected = this->root[L"navi-apps"][L"selected"].AsInt();
    return this->root[L"navi-apps"][L"list"][selected][L"bin"].AsString();
  }
  
  ::wxString Config::GetNaviName () {
    int selected = this->root[L"navi-apps"][L"selected"].AsInt();
    return this->root[L"navi-apps"][L"list"][selected][L"name"].AsString();
  }

  Config::~Config () {
  }
}