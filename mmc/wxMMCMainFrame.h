#pragma once
#include <wx/wx.h>
#include "wxNavi.h"
#include "wxMMCFrame.h"

namespace mmc {

  class wxMMCMainFrame : public wxMMCFrame {
    public:
      wxMMCMainFrame ();
      ~wxMMCMainFrame ();

    protected:
      ::wxTextCtrl *m_log;
      ::wxStaticText *m_sysInfoLabel;
      ::wxStaticText *m_timeLabel;
      wxNavi *m_naviPrc;

    protected:
      void OnSysInfoUpdate (wxTimerEvent& e);

      void OnExit (wxCommandEvent &e);
      void OnRadio (wxCommandEvent &e);
      void OnConsole (wxCommandEvent &e);
      void OnMedia (wxCommandEvent &e);
      void OnMinimize (wxCommandEvent &e);
      void OnNavi (wxCommandEvent &e);
      
      void OnTest0Action (wxCommandEvent &e);
      void OnTest1Action (wxCommandEvent &e);

      DECLARE_EVENT_TABLE()
    
  };

}
