#include <wx/stdpaths.h>
#include <wx/filename.h>
#include <wx/wfstream.h>
#include "wxMMCRadio.h"
#include "wxApiHandler.h"
#include "wxMMCApp.h"

namespace mmc {

  BEGIN_EVENT_TABLE(wxMMCRadio, ::wxEvtHandler)
    EVT_TIMER(TIMER_KEY_LONG_PRESS, wxMMCRadio::OnLongPress)
  END_EVENT_TABLE()

  #include <wx/arrimpl.cpp> // this is a magic incantation which must be done!
  WX_DEFINE_OBJARRAY(wxMMCRadioStationArray);

  wxMMCRadio::wxMMCRadio () : ::wxEvtHandler () {
    ::wxFileInputStream radioJSON(::wxFileName(::wxStandardPaths::Get().GetExecutablePath()).GetPath()+L"/../res/radio.json");
    ::wxJSONReader reader;
    this->m_currentStation = 0;
    if (0 == reader.Parse(radioJSON, &this->m_config)) {
      const ::wxJSONInternalArray *arr = this->m_config[L"stations"].AsArray();
      for (size_t i = 0; i < arr->size(); i++) {
        this->m_stations.Add(new wxMMCRadioStation(
          (wxMMCRadioStation::RadioBand)arr->Item(i)[L"band"].AsInt(),
          arr->Item(i)[L"fq"].AsInt(),
          arr->Item(i)[L"title"].AsString(),
          arr->Item(i)[L"img"].AsString()
        ));
      }
    }
    else {
      ::wxArrayString errs = reader.GetErrors();
      for (size_t i = 0; i < errs.Count(); i++) {
        ::wxLogFatalError(L"Radio config err: %s", errs[i]);
      }
    }

    this->m_pressTimer = new ::wxTimer(this, TIMER_KEY_LONG_PRESS);
    this->m_longPress = false;
  }
  
  void wxMMCRadio::KeyListener (wxApiEvent &e) {
    switch (e.GetApiWPARAM()) {
      case ::MMCKey::Nxt:
      case ::MMCKey::Prv: {
        if (::MMCKey::Down == e.GetApiLPARAM()) {
          this->m_pressTimer->Stop();
          this->m_longPress = false;
          this->m_pressTimer->Start(LONG_PRESS_TIME, true);
        }
        if (::MMCKey::Up == e.GetApiLPARAM()) {
          if (!this->m_longPress) {
            if (e.GetApiWPARAM() == ::MMCKey::Nxt) {
              this->Next();
            }
            if (e.GetApiWPARAM() == ::MMCKey::Prv) {
              this->Prev();
            }
          }
          else {
            this->Search(e.GetApiLPARAM() == ::MMCKey::Prv);
          }
        }
      } break;
    }
  }
  
  void wxMMCRadio::OnLongPress (::wxTimerEvent &e) {
    this->m_longPress = true;
  }

  wxMMCRadioStation::RadioBand wxMMCRadio::Band (wxMMCRadioStation::RadioBand b) {
    if (wxMMCRadioStation::BAND != b) {
      ::Radio_SetBand((DWORD)b);
    }
    return (wxMMCRadioStation::RadioBand)::Radio_GetCurrentBand();
  }
  
  wxMMCRadio::SrchLevel wxMMCRadio::SearchLevel (SrchLevel l) {
    if (LEVEL != l) {
      ::Radio_SetSearchLevel((int)l);
    }
    return (SrchLevel)::Radio_GetSearchLevel();
  }

  void wxMMCRadio::Search (bool down, SrchMode mode) {
    ::Radio_AutoSearch(down);
  }
  
  bool wxMMCRadio::IsStereo () {
    ::wxLogMessage(wxT("StereoMsg %s"), ::Radio_GetStereoMsg());
    ::wxLogMessage(wxT("StereoStatus %d"), ::Radio_GetStereoStatus());
    return ::Radio_GetStereoMsg() > 0;
  }
  
  DWORD wxMMCRadio::Freq (DWORD f) {
    if (0 != f) {
      ::Radio_SetFreq(f);
    }
    return (DWORD)::Radio_GetCurrentFreq();
  }
  
  int wxMMCRadio::Station (int s) {
    if (s != -1) {
      s %= m_stations.Count();
      this->Band(this->m_stations[s].band);
      this->Freq(this->m_stations[s].fq);
      this->m_currentStation = s;
    }
    return this->m_currentStation;
  }

  const wxMMCRadioStationArray& wxMMCRadio::GetStations () {
    return this->m_stations;
  }

  void wxMMCRadio::Run () {
    //bind events
    this->Connect(wxApiHandler::UWM_KEY, wxAPI_EVENT, wxApiEventHandler(wxMMCRadio::KeyListener));
    wxApiHandler::Instance().State(wxApiHandler::Radio);
  }

  void wxMMCRadio::Stop () {
    //unbind events
    this->Disconnect(wxApiHandler::UWM_KEY, wxAPI_EVENT, wxApiEventHandler(wxMMCRadio::KeyListener));
    wxApiHandler::Instance().State(wxApiHandler::Media);
  }
  
  void wxMMCRadio::Next (wxApiEvent &e) {
    this->Station(this->Station()+1);
  }
  
  void wxMMCRadio::Prev (wxApiEvent &e) {
    this->Station((this->Station() ? this->Station() : this->m_stations.Count())-1);
  }

  wxMMCRadio::~wxMMCRadio () {
    this->Stop();
  }
}