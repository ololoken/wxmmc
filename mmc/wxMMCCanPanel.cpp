#include "wxMMCCanPanel.h"
#include "wxMMCResource.h"
#include "wxMMCApp.h"
#include "wxMMCSkin.h"
#include <wx/listimpl.cpp>

namespace mmc {
  WX_DEFINE_LIST(CanPanelItemsList);

  BEGIN_EVENT_TABLE(wxMMCCanPanel, wxMMCFrame)
    EVT_MMCCAN(WM_CAN_CHANGE_ACTIVE, wxMMCCanPanel::Active)
    EVT_MMCCAN(WM_CAN_CHANGE_ENGINE_TEMP, wxMMCCanPanel::UpdateEngineTemp)
    EVT_MMCCAN(WM_CAN_CHANGE_GEARBOX_TEMP, wxMMCCanPanel::UpdateGearboxTemp)
    EVT_MMCCAN(WM_CAN_CHANGE_IN_CAR_TEMP, wxMMCCanPanel::UpdateInCarTemp)
    EVT_MMCCAN(WM_CAN_CHANGE_AIR_TEMP, wxMMCCanPanel::UpdateOutCarTemp)
    EVT_MMCCAN(WM_CAN_CHANGE_FUEL, wxMMCCanPanel::UpdateFuel)
    EVT_MMCCAN(WM_CAN_CHANGE_FUEL_INSTANT_CONSUMPTION, wxMMCCanPanel::UpdateFuelInstantConsumption)
    EVT_MMCCAN(WM_CAN_CHANGE_SPEED, wxMMCCanPanel::UpdateSpeed)
    EVT_MMCCAN(WM_CAN_CHANGE_DISTANCE, wxMMCCanPanel::UpdateDistance)
  END_EVENT_TABLE()
  
  const ::wxString wxMMCCanPanel::AvailableItems[] = {
    L"engine-temp",
    L"gearbox-temp",
    L"incar-temp",
    L"outcar-temp",
    L"fuel-total",
    L"fuel-instant",
    L"speed",
    L"distance"
  };

  wxMMCCanPanel::wxMMCCanPanel () : wxMMCFrame(NULL, L"can-panel", wxPoint(0, 440), wxSize(800, 40), wxBORDER_NONE|wxSTAY_ON_TOP) {
    this->m_cpItems.SetKeyType(::wxKEY_STRING);
    ::wxArrayString items = wxGetApp().Skin->GetFrameItemNames(this);
    int i = items.Count();
    while (i--) {
      int idx = this->ItemIndex(items.Item(i));
      if (wxNOT_FOUND == idx) {
        continue;
      }
      this->m_cpItems.Append(AvailableItems[idx].c_str(), wxGetApp().Skin->GetCanPanelItem(this, AvailableItems[idx]));
    }
  }

  wxMMCCanPanel::~wxMMCCanPanel () {}

  int wxMMCCanPanel::ItemIndex (const ::wxString& itemName) {
    static const int sz = sizeof(AvailableItems)/sizeof(::wxString);
    for (int i = 0; i < sz; i++) {
      if (AvailableItems[i] == itemName) return i;
    }
    return wxNOT_FOUND;
  }

  void wxMMCCanPanel::UpdateEngineTemp (wxCanEvent &e) {
    this->UpdateItemLabel(L"engine-temp", ::wxString::Format(L"%d", e.GetCanData()+CAN_ECU_TEMP_OFFSET));
  }
  
  void wxMMCCanPanel::UpdateInCarTemp (wxCanEvent &e) {
    this->UpdateItemLabel(L"incar-temp", ::wxString::Format(L"%d", e.GetCanData()+CAN_ECU_TEMP_OFFSET));
  }

  void wxMMCCanPanel::UpdateGearboxTemp (wxCanEvent &e) {
    this->UpdateItemLabel(L"gearbox-temp", ::wxString::Format(L"%d", e.GetCanData()+CAN_ECU_TEMP_OFFSET));
  }
  
  void wxMMCCanPanel::UpdateOutCarTemp (wxCanEvent &e) {
    this->UpdateItemLabel(L"outcar-temp", ::wxString::Format(L"%d", e.GetCanData()+CAN_ECU_TEMP_OFFSET));
  }
  
  void wxMMCCanPanel::UpdateFuel (wxCanEvent &e) {
    this->UpdateItemLabel(L"fuel-total", ::wxString::Format(L"%.1f", (float)e.GetCanData()/CAN_ECU_FUEL_TOTAL_MULTIPLIER));
  }
  
  void wxMMCCanPanel::UpdateFuelInstantConsumption (wxCanEvent &e) {
    ::wxString fi = ::wxString::Format(L"%.1f", ((float)e.GetCanData())/CAN_ECU_FUEL_CONSUPTION_MULTIPLIER);
    this->UpdateItemLabel(L"fuel-instant", fi);
  }
  
  void wxMMCCanPanel::UpdateDistance (wxCanEvent &e) {
    this->UpdateItemLabel(L"distance", ::wxString::Format(L"%d", e.GetCanData()));
  }
  
  void wxMMCCanPanel::UpdateSpeed (mmc::wxCanEvent &e) {
    static int speed = 0;
    int spd = int(0.5f+((float)e.GetCanData())/CAN_ECU_SPEED_MULTIPLIER);
    if (spd != speed) {
      speed = spd;
      this->UpdateItemLabel(L"speed", ::wxString::Format(L"%d", speed));
    }
  }
  
  void wxMMCCanPanel::UpdateItemLabel (const ::wxString& itemName, const ::wxString& label) {
    wxCanPanelItemsListNode* cpiNode = this->m_cpItems.Find(wxListKey(itemName.c_str()));
    if (cpiNode) {
      cpiNode->GetData()->SetItemValue(label);
    }
  }

  void wxMMCCanPanel::Active(wxCanEvent &e) {
    this->Show(1 == e.GetCanData());
  }
  
  void wxMMCCanPanel::RelayEvent (::wxEvent &e) {}


}