#pragma once;
#include <wx/wx.h>
#include <wx/jsonreader.h>
#include "wxMMCFrame.h"
#include "wxMMCButton.h"
#include "wxMMCCanPanelItem.h"

namespace mmc {
  
  class wxMMCSkin : public ::wxObject {
    public:
      wxMMCSkin (const wxString& path);
      ~wxMMCSkin ();
      
    public:
      wxMMCButton* GetButton (::wxFrame *frame, const wxString& button, wxWindowID id = wxID_ANY);
      ::wxStaticText* GetLabel (::wxFrame *frame, const wxString& label, wxWindowID id = wxID_ANY, const wxString& text = L"");
      ::wxBitmap GetBackground (::wxFrame *frame);
      ::wxPanel* GetPanel (::wxFrame *frame, const wxString& panel);
      wxMMCCanPanelItem* GetCanPanelItem (::wxFrame *frame, const wxString& item, wxWindowID id = wxID_ANY, const wxString& text = L"");
      ::wxArrayString GetFrameItemNames (::wxFrame *frame);
      void Setup (wxMMCFrame *frame);
      void SetPosition (::wxWindow *wnd, ::wxJSONValue &pos);
      void SetFont (::wxWindow *wnd, ::wxJSONValue &fnt);

    protected:
      wxJSONValue root;
  };
  
}