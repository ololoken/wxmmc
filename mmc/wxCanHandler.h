#pragma once

#include <wx/wx.h>
#include "mmc_can.h"
#include "wxCanEvent.h"

namespace mmc {

  class wxCanHandler: public ::wxThread {
    public:
      static wxCanHandler& Instance () {
        static wxCanHandler instance;
        return instance;
      };
      wxThreadError Run ();
      
      ~wxCanHandler ();
      
      ::VEHICLE GetVehicle ();
      

    protected:
      HWND m_HWND;
    
      wxCanHandler();
      void operator= (wxCanHandler const&);
      
    protected:
      virtual ExitCode Entry();
      static LRESULT CALLBACK Receiver (HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
  };

}