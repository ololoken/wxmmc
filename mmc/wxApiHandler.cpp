#include "wxApiHandler.h"
#include "wxApiEvent.h"
#include "wxMMCApp.h"

namespace mmc {

  wxApiHandler::wxApiHandler (void) : ::wxThread(::wxTHREAD_DETACHED) {
    this->MMC_MESSAGES_COUNT = sizeof(::MMC_MESSAGES)/sizeof(::MMC_MESSAGES[0]);
    this->RegisterMessages();
    this->m_HWND = ::CreateWindowEx(0, L"Static", L"ApiHandlerMessageWindow", 0, 0, 0, 0, 0, 0, NULL, ::wxGetInstance(), NULL);
    ::SetWindowLong(this->m_HWND, GWL_WNDPROC, (long)&wxApiHandler::Receiver);
    ::SetWindowLong(this->m_HWND, GWL_USERDATA, (long)this);
    this->Create();
    ::wxLogMessage(wxT("MMC: DLL v%s"), ::Api_GetDLLversion());
  }

  LRESULT CALLBACK wxApiHandler::Receiver (HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
    wxApiHandler* h = (wxApiHandler*)GetWindowLong(hwnd, GWL_USERDATA);
    MMC_MESSAGE mmcMsg = h->MMCMessage(msg);
    if (wxNOT_FOUND != mmcMsg) {
      if (UWM_MCU_TIME_UPDATA != mmcMsg) {
        ::wxLogMessage(wxT("MMC: msg %s#%d#%d, W%d, W%d"), ::MMC_MESSAGES[(size_t)(mmcMsg-MMC_MSG_OFFSET)], msg, mmcMsg, wParam, lParam);
      }
      wxApiEvent(mmcMsg, wParam, lParam).Broadcast();
    }
    return 0;
  }
  
  wxThread::ExitCode wxApiHandler::Entry () {
    ::wxLogMessage(wxT("MMC: MMC21_init %d"), ::MMC21_init());
    ::wxLogMessage(wxT("MMC: Api_SetHwnd %d"), ::Api_SetHwnd(this->m_HWND));
    this->State(Media);
    while (!this->TestDestroy()) {
      this->Sleep(50);
    }
    ::MMC21_uninit();
    return (wxThread::ExitCode)0;
  }
  
  void wxApiHandler::RegisterMessages () {
    for (size_t i = 0; i < this->MMC_MESSAGES_COUNT; i++) {
      this->m_messages[i] = ::RegisterWindowMessage(::MMC_MESSAGES[i]);
    }
  }
  
  wxApiHandler::MMC_MESSAGE wxApiHandler::MMCMessage (UINT msg) {
    for (size_t i = 0; i < this->MMC_MESSAGES_COUNT; i++) {
      if (this->m_messages[i] == msg) {
        return (MMC_MESSAGE)(i+MMC_MSG_OFFSET);
      }
    }
    return (MMC_MESSAGE)wxNOT_FOUND;
  }

  wxApiHandler::MMC_STATE wxApiHandler::State (MMC_STATE state = Current) {
    if (Current != state) {
      ::Api_SetActivateMode((DWORD)state);
    }
    return (MMC_STATE)::Api_GetActivateMode();
  }

  wxApiHandler::~wxApiHandler (void) {
    ::DestroyWindow(this->m_HWND);
  }

}