#pragma once
#include <wx/wx.h>
#include "bt_nfore.h"
#include "constants.h"
#include "wxBtEvent.h"

namespace mmc {

  class wxBtHandler: public ::wxThread {
    public:
      static wxBtHandler& Instance () {
        static wxBtHandler instance;
        return instance;
      };
      
      ~wxBtHandler ();

    protected:
      HWND m_HWND;

    protected:
      wxBtHandler();
      void operator= (wxBtHandler const&);

      virtual ExitCode Entry();
      static LRESULT CALLBACK Receiver (HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

    public:
      static void OnError (int err_code);
  };

}