#include <wx/wx.h>
#include "wxMMCButton.h"
#include "wxMMCFrame.h"

namespace mmc {

  BEGIN_EVENT_TABLE(wxMMCButton, wxBitmapButton)
    EVT_PAINT(wxMMCButton::OnPaint)
    EVT_ERASE_BACKGROUND(wxMMCButton::OnEraseBg)
    EVT_LEFT_DOWN(wxMMCButton::OnMouseDown)
  END_EVENT_TABLE()

  wxMMCButton::wxMMCButton (wxWindow* parent, wxWindowID id, wxBitmap& normal, wxBitmap& pressed, const wxPoint& pos = wxDefaultPosition, const wxString& label) : wxBitmapButton(parent, id, normal, pos, wxDefaultSize, wxNO_BORDER) {
    this->SetBitmapSelected(pressed);
    this->m_isSelected = false;
    this->SetWindowStyle(this->GetWindowStyle() | wxTRANSPARENT_WINDOW);
    if (!label.IsEmpty()) {
      this->SetLabel(label);
    }
  }
  
  void wxMMCButton::OnPaint (::wxPaintEvent &e) {
    ::wxPaintDC pdc(this);
    ::wxBitmap bmp = this->m_isSelected ? this->GetBitmapSelected() : this->GetBitmapLabel();
    pdc.DrawBitmap(bmp, 0, 0, true);
    ::wxString label = this->GetLabelText();
    if (!label.IsNull()) {
      pdc.SetFont(this->GetFont());
      pdc.SetTextForeground(this->GetForegroundColour());
      pdc.DrawText(label, bmp.GetWidth()+2, (bmp.GetHeight()-this->GetFont().GetPointSize())/2);
    }
  }
  
  void wxMMCButton::OnEraseBg (::wxEraseEvent &e) {
    wxMMCFrame *f = wxDynamicCast(this->GetParent(), wxMMCFrame);
    if (NULL != f) {
      f->DrawBackground(e.GetDC(), ::wxRect(this->GetPosition(), this->GetSize()));
    }
  }
  
  bool wxMMCButton::MSWOnDraw (WXDRAWITEMSTRUCT *item) {
    this->m_isSelected = (((LPDRAWITEMSTRUCT)item)->itemState & ODS_SELECTED) != 0;
    return ::wxBitmapButton::MSWOnDraw(item);
  }
  
  void wxMMCButton::OnMouseDown (::wxMouseEvent &e) {
    this->Refresh();
    e.Skip();
  }

}