#pragma once
#include <wx/wx.h>

namespace mmc {

  class wxMMCDashboard : public wxMMCFrame {
    public:
      wxMMCDashboard ();
      ~wxMMCDashboard ();

    protected:
    

    protected:
      void OnExit (wxCommandEvent& e);

      DECLARE_EVENT_TABLE()
  };

}
