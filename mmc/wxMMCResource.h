#pragma once

#include <wx/wx.h>

namespace mmc {

  class wxMMCResource {
    protected:
      wxMMCResource ();
      
      static ::wxArrayString fontPaths;
  
    public:
      static ::wxBitmap Get (const wxString& path);
      static int FontRegister (const ::wxString& fpath);
      static void FontsRegister (const ::wxArrayString& fpaths);
      static void FontsFree ();
  };

}