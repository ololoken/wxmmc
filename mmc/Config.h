#pragma once;
#include <wx/wx.h>
#include <wx/jsonreader.h>

namespace mmc {
  
  class Config : public ::wxObject {
    public:
      Config (const wxString& path);
      ~Config ();
      
    public:
      ::wxJSONValue Get (const ::wxString &name);
      ::wxString GetNaviPath ();
      ::wxString GetNaviName ();

    protected:
      ::wxJSONValue root;
  };
  
}