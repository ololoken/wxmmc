#include <wx/stdpaths.h>
#include <wx/filename.h>
#include "wxMMCResource.h"

namespace mmc {

  ::wxArrayString wxMMCResource::fontPaths = ::wxArrayString();

  wxBitmap wxMMCResource::Get (const wxString& path) {
    const ::wxString& fpath = ::wxFileName(::wxStandardPaths::Get().GetExecutablePath()).GetPath()+wxT("/../")+path;
    ::wxBitmap res = ::wxBitmap(::wxImage(fpath));
    return res;
  }
  
  void wxMMCResource::FontsRegister (const ::wxArrayString& fpaths) {
    for (size_t i = 0; i < fpaths.Count(); i++) {
      FontRegister(fpaths[i]);
    }
  }
  
  int wxMMCResource::FontRegister (const ::wxString& fontPath) {
    const ::wxString& p = ::wxFileName(::wxStandardPaths::Get().GetExecutablePath()).GetPath()+wxT("/../")+fontPath;
    fontPaths.Add(fontPath);
    return ::AddFontResource(p.c_str());
  }
  
  void wxMMCResource::FontsFree () {
    for (size_t i = 0; i < fontPaths.Count(); i++) {
      const ::wxString& p = ::wxFileName(::wxStandardPaths::Get().GetExecutablePath()).GetPath()+wxT("/../")+fontPaths[i];
      ::RemoveFontResource(p.c_str());
    }
  }
}