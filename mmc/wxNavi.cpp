#include "wxNavi.h"
#include "constants.h"
#include "wxApiHandler.h"
#include <wx/stdpaths.h>
#include <wx/filename.h>
#include <wx/wfstream.h>

namespace mmc {

  BEGIN_EVENT_TABLE(wxNavi, ::wxProcess)
    EVT_TIMER(TIMER_NAVIWND_UPDATE, wxNavi::OnTimer)
  END_EVENT_TABLE()

  wxNavi::wxNavi () : ::wxProcess () {
    this->m_timer = new ::wxTimer(this, TIMER_NAVIWND_UPDATE);
    this->m_timer->Start(10000);
  }
  
  wxNavi::~wxNavi () {}
  
  void wxNavi::OnTerminate (int pid, int status) {

  }
  
  bool wxNavi::IsRunning () {
    return ::wxProcess::Exists(this->GetPid());
  }

  void wxNavi::Run (const ::wxString &path, const ::wxString &name) {
    this->m_pid = ::wxExecute(path, wxEXEC_ASYNC, this);
  }
  
  BOOL wxNavi::EnumProc (HWND hWnd, LPARAM lParam) {
    wxNavi *nav = (wxNavi *)lParam;
    DWORD pid = 0;
    ::GetWindowThreadProcessId(hWnd, &pid);
    if (nav->GetPid() == pid) {
      ::SetWindowPos(hWnd, HWND_TOP, 0, 0, 800, 440, NULL);
      return FALSE;
    }
    return TRUE;
  }
  
  void wxNavi::OnTimer (::wxTimerEvent &e) {
    if (this->IsRunning()) {
      ::EnumWindows(&wxNavi::EnumProc, (LPARAM)this);
    }
  }

}