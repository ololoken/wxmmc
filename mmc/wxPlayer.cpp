#include "wxPlayer.h"
//#include <wx/log.h>

namespace mmc {
  BEGIN_EVENT_TABLE(wxPlayer, ::wxEvtHandler)
  END_EVENT_TABLE()

  bool wxPlayer::Init (::wxWindow* po) {
    this->m_playerOutput = po;
    if (::Context_Init((tchar_t *)T("wxPlayer"), (tchar_t *)T("0.1"), 3, (tchar_t *)T(""), NULL)) {
      this->m_player = (player *)::Context()->Player;
      ::Context_Wnd((void *)this->m_playerOutput->GetHWND());
      this->Connect(::wxEVT_SIZE, wxSizeEventHandler(wxPlayer::OnOutputSize));
      this->UpdateSize();
      return true;
    }
    return false;
  }
  
  void wxPlayer::DeInit () {
    ::Context_Done();
    Context()->Error.Func = NULL;
  }
  
  void wxPlayer::Play (::wxString url) {
    int n = 1, idx = 0;
    this->m_player->Set(this->m_player, PLAYER_LIST_COUNT, &n, sizeof(n));
    ::PlayerAdd(this->m_player, idx, (tchar_t *)((wchar_t *)url.wchar_str()), NULL);
    this->m_player->Set(this->m_player, PLAYER_LIST_CURRIDX, &idx, sizeof(idx));
    this->Play();
  }
  
  void wxPlayer::Play (bool play) {
    int n = play ? 1 : 0;
    this->m_player->Set(this->m_player, PLAYER_PLAY, &n, sizeof(n));
  }
  
  void wxPlayer::Pause () {
    this->Play(false);
  }
  
  void wxPlayer::UpdateSize () {
    ::wxSize sz = this->m_playerOutput->GetSize();
    ::wxPoint pos = this->m_playerOutput->GetPosition();
    rect vp = {pos.x, pos.y, sz.x, sz.y};
    this->m_player->Set(this->m_player, PLAYER_SKIN_VIEWPORT, &vp, sizeof(rect));
    this->m_player->Set(this->m_player, PLAYER_UPDATEVIDEO, NULL, 0);
  }
  
  void wxPlayer::OnOutputSize (::wxSizeEvent &e) {
    this->UpdateSize();
  }

}