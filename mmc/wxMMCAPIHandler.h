#pragma once

#include <wx/wx.h>


namespace mmc {

  class wxMMCApiHandler : public ::wxThread {
    public:
      static wxMMCApiHandler& getInstance () {
        static wxMMCApiHandler instance;
        return instance;
      };

      ~wxMMCApiHandler (void);

    protected:
      wxMMCApiHandler ();
      void operator= (wxMMCApiHandler const&);
      virtual ExitCode Entry ();
      static LRESULT CALLBACK Receiver (HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
    
    protected:
      HWND m_HWND;
  };

}
