#include <wx/sysopt.h>
#include "wxMMCApp.h"
#include "wxCanHandler.h"
#include "wxApiHandler.h"
#include "wxBtHandler.h"
#include "wxMMCRadioFrame.h"

namespace mmc {

  IMPLEMENT_APP(wxMMCApp)

  bool wxMMCApp::OnInit () {
    ::wxImage::AddHandler(new ::wxJPEGHandler());
    ::wxImage::AddHandler(new ::wxPNGHandler());
    this->activeFrameId = 0;
    this->Cfg = new Config(L"res/config.json");
    this->Skin = new wxMMCSkin(this->Cfg->Get(L"skin-file").AsString());


    this->frame = new wxMMCMainFrame();
    this->panel = new wxMMCCanPanel();
    this->frame->Show();

    wxBtHandler::Instance().Run();
    wxCanHandler::Instance().Run();
    wxApiHandler::Instance().Run();

    this->Radio = new wxMMCRadio();
    
    this->Connect(wxApiHandler::UWM_KEY, wxAPI_EVENT, wxApiEventHandler(wxMMCApp::KeyListener));

    return true;
  }
  
  wxMMCCanPanel* wxMMCApp::GetCANPanel () {
    return this->panel;
  }

  void wxMMCApp::Broadcast (::wxEvent& e) {
    ::wxPostEvent(this->panel, e);
    ::wxPostEvent(this->frame, e);
    ::wxPostEvent(this->Radio, e);
  }
  
  void wxMMCApp::ShowFrame (const ::wxString &name) {
    ::wxWindow *wnd = ::wxWindow::FindWindowByLabel(name, this->frame);
    if (!wnd && name == L"radio") {
      wnd = (::wxWindow *)new wxMMCRadioFrame(this->frame, this->Radio);
    }
    else if (!wnd && name == L"player") {
      wnd = (::wxWindow *)new wxPlayerFrame(this->frame);
    }
    this->activeFrameId = wnd->GetId();
    wnd->Show();
  }

  void wxMMCApp::KeyListener (wxApiEvent &e) {
    switch (e.GetApiWPARAM()) {
      case ::MMCKey::Radio: {
        if (!e.GetApiLPARAM()) this->ShowFrame(L"radio");
      } break;
      case ::MMCKey::Media: {
        if (!e.GetApiLPARAM()) this->ShowFrame(L"player");
      } break;
    }
  }

  int wxMMCApp::OnExit () {
    delete this->Skin;
    delete this->Radio;

    wxBtHandler::Instance().Delete();
    wxCanHandler::Instance().Delete();
    wxApiHandler::Instance().Delete();

    return ::wxApp::OnExit();
  }

}