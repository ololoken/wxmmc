#include "wxCanHandler.h"
#include "wxCanEvent.h"

namespace mmc {

  wxCanHandler::wxCanHandler () : ::wxThread(::wxTHREAD_DETACHED) {
    m_HWND = ::CreateWindowEx(0, L"Static", L"CANHandlerMessageWindow", 0, 0, 0, 0, 0, 0, NULL, ::wxGetInstance(), NULL);
    ::SetWindowLong(this->m_HWND, GWL_WNDPROC, (long)&wxCanHandler::Receiver);
    ::SetWindowLong(this->m_HWND, GWL_USERDATA, (long)this);
    this->Create();
  }

  wxThreadError wxCanHandler::Run () {
    return wxThread::Run();
  }

  wxCanHandler::~wxCanHandler () {
    ::DestroyWindow(this->m_HWND);
  }

  LRESULT CALLBACK wxCanHandler::Receiver (HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
    wxCanHandler* ch = (wxCanHandler*)GetWindowLong(hwnd, GWL_USERDATA);
    switch (msg) {
      case WM_CAN_CHANGE_IN_CAR_TEMP:
      case WM_CAN_CHANGE_GEARBOX_TEMP:
      case WM_CAN_CHANGE_ENGINE_TEMP:
      case WM_CAN_CHANGE_AIR_TEMP:
      case WM_CAN_RUNNING_STATE:
      case WM_CAN_CHANGE_ACTIVE:
      case WM_CAN_PORT_EMULATOR:
      case WM_CAN_CHANGE_FUEL_INSTANT_CONSUMPTION:
      case WM_CAN_CHANGE_FUEL:
      case WM_CAN_CHANGE_SPEED:
      case WM_CAN_CHANGE_DISTANCE:
        wxCanEvent(msg, (int)wParam).Broadcast();
        break;
    }
   
    return 0;
  }

  wxThread::ExitCode wxCanHandler::Entry() {
    ::CAN_Init(this->m_HWND);
    while (!this->TestDestroy()) {
      this->Sleep(50);
    }
    ::CAN_Close();
    return (wxThread::ExitCode)0;
  }

  ::VEHICLE wxCanHandler::GetVehicle () {
    ::VEHICLE v;
    ::CAN_GetVehicleData(&v);
    return v;
  }

}