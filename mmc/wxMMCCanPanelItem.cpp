#include "wxMMCCanPanelItem.h"

namespace mmc {

  const ::wxSize wxMMCCanPanelItem::DefaultSize = ::wxSize(100, 45);

  wxMMCCanPanelItem::wxMMCCanPanelItem (wxWindow* parent, const wxBitmap& bm, const wxString& label, const wxPoint &pos, const wxSize &size) : ::wxPanel(parent, wxID_ANY, pos, size) {
    this->m_labelOffset = ::wxDefaultPosition;
    if (size == ::wxDefaultSize) {
      this->SetSize(DefaultSize);
    }

    this->m_icon = new ::wxStaticBitmap(this, wxID_ANY, bm);
    this->m_labelFormat = label;
    this->m_label = new ::wxStaticText(this, wxID_ANY, L"--", this->GetLabelPos());

    //inherit colors from parent, font will be inherited automatically
    this->SetBackgroundColour(wxColour(this->GetParent()->GetBackgroundColour()));
    this->m_label->SetBackgroundColour(wxColour(this->GetParent()->GetBackgroundColour()));
    this->m_label->SetForegroundColour(wxColour(this->GetParent()->GetForegroundColour()));
  }
  
  void wxMMCCanPanelItem::SetItemLabelOffset (::wxPoint p) {
    this->m_labelOffset = p;
    this->m_label->SetPosition(this->GetLabelPos());
  }
  
  ::wxPoint wxMMCCanPanelItem::GetLabelPos () {
    return this->m_labelOffset+this->m_icon->GetRect().GetTopRight();
  }

  ::wxStaticText* wxMMCCanPanelItem::GetLabel () {
    return this->m_label;
  }

  void wxMMCCanPanelItem::SetItemIcon (const wxBitmap& bm) {
    this->m_icon->SetBitmap(bm);
    this->m_label->SetPosition(this->GetLabelPos());
  }

  void wxMMCCanPanelItem::SetItemValue (const wxString& label) {
    this->m_label->SetLabel(::wxString::Format(this->m_labelFormat, label));
  }

}