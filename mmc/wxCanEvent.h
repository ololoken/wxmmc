#pragma once

#include <wx/wx.h>

DECLARE_EVENT_TYPE(wxCAN_EVENT, wxID_ANY);

#define EVT_MMCCAN(id, fn) \
  DECLARE_EVENT_TABLE_ENTRY(wxCAN_EVENT, id, wxID_ANY, (wxObjectEventFunction)(wxEventFunction)&fn, (wxObject*)NULL),

namespace mmc {

  class wxCanEvent: public ::wxEvent {
    public:
      wxCanEvent ();
      wxCanEvent (const wxCanEvent &event);
      wxCanEvent (int id, int data);
      ~wxCanEvent ();

      virtual wxEvent *Clone() const { 
        return new wxCanEvent(*this); 
      }
      
      void Broadcast ();
      
      int GetCanData() {return m_canData;}
    protected:
      int m_canData;

      DECLARE_DYNAMIC_CLASS(wxCanEvent)
  };

  typedef void (wxEvtHandler::*wxCanEventFunction)(wxCanEvent&);

  #define wxCanEventHandler(func) \
    (wxObjectEventFunction)(wxCanEventFunction)wxStaticCastEvent(wxCanEventFunction, &func)
}