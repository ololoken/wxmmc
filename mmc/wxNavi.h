#pragma once
#include <wx/wx.h>
#include <wx/process.h>

namespace mmc {

  class wxNavi : public ::wxProcess {
    public:
      wxNavi ();
      ~wxNavi ();
      
      void OnTerminate (int pid, int status);
      void Run (const ::wxString &path, const ::wxString &name);
      bool IsRunning ();

    protected:
      ::wxTimer *m_timer;
    
    protected:
      void OnTimer (::wxTimerEvent &e);
      static BOOL EnumProc (HWND, LPARAM);

      DECLARE_EVENT_TABLE()
    
  };

}
