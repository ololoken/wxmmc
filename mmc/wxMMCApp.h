#pragma once

#include <wx/wx.h>
#include <wx/log.h>
#include "wxMMCMainFrame.h"
#include "wxMMCCanPanel.h"
#include "wxMMCSkin.h"
#include "Config.h"
#include "wxMMCRadio.h"
#include "wxPlayerFrame.h"

namespace mmc {

  class wxMMCApp : public wxApp {
    protected:
      wxMMCMainFrame *frame;
      wxMMCCanPanel *panel;
      ::wxWindowID activeFrameId;
      
    protected:
      int OnExit ();

    public:
      wxMMCSkin *Skin;
      wxMMCRadio *Radio;
      Config *Cfg;
    public:
      bool OnInit ();
      wxMMCCanPanel* wxMMCApp::GetCANPanel ();
      void Broadcast (::wxEvent& e);
      void ShowFrame (const ::wxString& name);
      void KeyListener (wxApiEvent &e);
      
    
    private:
  };

  DECLARE_APP(wxMMCApp)

}