#pragma once
#include <wx/wx.h>
#include "wxMMCRadio.h"
#include "wxApiEvent.h"
#include "wxMMCFrame.h"
#include "wxMMCButton.h"

namespace mmc {

  class wxMMCRadioFrame : public wxMMCFrame {
    public:
      wxMMCRadioFrame (::wxWindow *parent, wxMMCRadio *radio);
      ~wxMMCRadioFrame ();

    protected:
      wxMMCRadio *m_radio;
      
      ::wxStaticText *m_freqLabel;
      ::wxPanel *m_stationsPanel;
      ::wxRadioBox *m_bandBox;
      ::wxRadioBox *m_srchLevelBox;
      wxMMCButton *m_btnBand;
      wxMMCButton *m_btnRange;
      int m_pageCurrent;
      int m_itemWidth;
      int m_perPage;

    protected:
      void DrawFreq ();
      void DrawStations (int page = -1);
      void DrawBand ();
      void DrawRange ();

      void OnExit (::wxCommandEvent& e);
      void OnMinimize (::wxCommandEvent &e);
      void OnIncFreq (::wxCommandEvent& e);
      void OnDecFreq (::wxCommandEvent& e);
      void OnStationChange (::wxCommandEvent& e);
      void OnChangeStationsPage (::wxCommandEvent &e);
      void OnStationNext (::wxCommandEvent &e);
      void OnStationPrev (::wxCommandEvent &e);
      void OnBandSelect (::wxCommandEvent& e);
      void OnSearchLevelSelect (::wxCommandEvent& e);
      
      void OnUpdateBand (wxApiEvent &e);
      void OnUpdateFreq (wxApiEvent &e);

      DECLARE_EVENT_TABLE()
  };

}
