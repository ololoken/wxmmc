#include <windows.h>
//todo: if function is required then load it from aygshell.dll dynamically
extern "C" {
  int SHInitExtraControls (void) {return false;}
  HWND SHFindMenuBar (HWND hWnd) {return NULL;}
  DWORD SHRecognizeGesture (void *data) {return 0;}
  BOOL SHInitDialog (void *data) {return false;}
  HWND SHHandleWMSettingChange (HWND hWnd, WPARAM wParam, LPARAM lParam, void *data) {return NULL;}
  BOOL SHHandleWMActivate (HWND hwnd, WPARAM wParam, LPARAM lParam, void *psai, DWORD dwFlags) {return false;}
  BOOL SHCreateMenuBar (void *data) {return false;}
}