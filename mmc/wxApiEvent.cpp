#include "wxApiEvent.h"
#include "wxMMCApp.h"

DEFINE_EVENT_TYPE(wxAPI_EVENT);

namespace mmc {

  IMPLEMENT_DYNAMIC_CLASS(wxApiEvent, wxEvent);

  wxApiEvent::wxApiEvent() : ::wxEvent(0, wxAPI_EVENT) {

  }

  wxApiEvent::wxApiEvent (int id, WPARAM wParam, LPARAM lParam) : ::wxEvent(id, wxAPI_EVENT) {
    this->m_apiWPARAM = wParam;
    this->m_apiLPARAM = lParam;
  }

  wxApiEvent::wxApiEvent (const wxApiEvent &event) : ::wxEvent(event) {
    this->m_apiWPARAM = event.m_apiWPARAM;
    this->m_apiLPARAM = event.m_apiLPARAM;
  }

  void wxApiEvent::Broadcast () {
    wxGetApp().Broadcast(*this);
  }

  wxApiEvent::~wxApiEvent () {}

}