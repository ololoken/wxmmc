#pragma once

#include <wx/wx.h>

DECLARE_EVENT_TYPE(wxAPI_EVENT, wxID_ANY);
#define EVT_MMCAPI(id, fn) \
  DECLARE_EVENT_TABLE_ENTRY(wxAPI_EVENT, id, wxID_ANY, (wxObjectEventFunction)(wxEventFunction)&fn, (wxObject*)NULL),

namespace mmc {

  class wxApiEvent: public ::wxEvent {
    public:
      wxApiEvent ();
      wxApiEvent (const wxApiEvent &event);
      wxApiEvent (int id, WPARAM wParam, LPARAM lParam);
      ~wxApiEvent ();

      virtual wxEvent *Clone() const { 
        return new wxApiEvent(*this); 
      }
      
      WPARAM GetApiWPARAM () {return this->m_apiWPARAM;}
      LPARAM GetApiLPARAM () {return this->m_apiLPARAM;}
      
      void Broadcast ();
    protected:
      WPARAM m_apiWPARAM;
      LPARAM m_apiLPARAM;

      DECLARE_DYNAMIC_CLASS(wxApiEvent)
  };

  typedef void (wxEvtHandler::*wxApiEventFunction)(wxApiEvent&);

  #define wxApiEventHandler(func) \
    (wxObjectEventFunction)(wxApiEventFunction)wxStaticCastEvent(wxApiEventFunction, &func)
}