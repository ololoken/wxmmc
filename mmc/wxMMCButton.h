#pragma once
#include <wx/wx.h>

namespace mmc {

  class wxMMCButton : public wxBitmapButton {

    public:
      wxMMCButton (wxWindow* parent, wxWindowID id, wxBitmap& normal, wxBitmap& pressed, const wxPoint& pos, const wxString& label = L"");
    protected:
      void OnEraseBg (::wxEraseEvent &e);
      void OnPaint (::wxPaintEvent &e);
      void OnMouseDown (::wxMouseEvent &e);
      
      bool MSWOnDraw(WXDRAWITEMSTRUCT *item);
      
      bool m_isSelected;

    private:
    
      DECLARE_EVENT_TABLE()
  };

}
