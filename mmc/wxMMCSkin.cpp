#include "wxMMCSkin.h"
#include "wxMMCResource.h"
#include <wx/stdpaths.h>
#include <wx/filename.h>
#include <wx/wfstream.h>

namespace mmc {
  wxMMCSkin::wxMMCSkin (const wxString& path) : ::wxObject() {
    ::wxFileInputStream skinJSON(::wxFileName(::wxStandardPaths::Get().GetExecutablePath()).GetPath()+wxT("/../")+path);
    ::wxJSONReader reader;
    if (0 == reader.Parse(skinJSON, &this->root)) {
      if (root[L"font-faces"].IsObject()) {
        ::wxArrayString faces = root[L"font-faces"].GetMemberNames();
        for (size_t i = 0; i < faces.Count(); i++) {
          wxMMCResource::FontRegister(root[L"font-faces"][faces[i]].AsString());
        }
      }
    }
    else {
      ::wxArrayString errs = reader.GetErrors();
      for (size_t i = 0; i < errs.Count(); i++) {
        ::wxLogFatalError(wxT("Skin err: %s"), errs[i]);
      }
    }
  }

  wxMMCButton* wxMMCSkin::GetButton (::wxFrame *frame, const wxString& button, wxWindowID id) {
    ::wxJSONValue b = root[frame->GetTitle()][L"buttons"][button];
    ::wxBitmap b0, b1;
    ::wxString label(L"");
    if (b[L"images"].IsArray()) {
      b0 =
      b1 = wxMMCResource::Get(b[L"images"][0].AsString());
      if (b[L"images"][1].IsValid()) {
        b1 = wxMMCResource::Get(b[L"images"][1].AsString());
      }
    }
    else if (b[L"images"].IsString()) {
      b0 = 
      b1 = wxMMCResource::Get(b[L"images"].AsString());
    }
    else {
      b0 = b1 = ::wxNullBitmap;
    }
    if (b[L"label"].IsString()) {
      label = b[L"label"].AsString();
    }
    wxMMCButton* res = new wxMMCButton(frame, id, b0, b1, ::wxDefaultPosition, label);
    res->SetName(button);
    this->SetPosition(res, b[L"position"]);
    this->SetFont(res, b[L"font"]);
    return res;
  }
  
  ::wxStaticText* wxMMCSkin::GetLabel (::wxFrame *frame, const wxString& label, wxWindowID id, const wxString& text) {
    ::wxJSONValue b = root[frame->GetTitle()][L"labels"][label];
    DWORD align = ::wxALIGN_LEFT;
    if (b[L"align"].AsString() == L"center") {//todo add css style aligning
      align = ::wxALIGN_CENTER;
    }
    else if (b[L"align"].AsString() == L"right") {
      align = ::wxALIGN_RIGHT;
    }
    ::wxStaticText* l = new ::wxStaticText(
      frame, id, text, 
      ::wxDefaultPosition, ::wxDefaultSize,
      align|wxST_NO_AUTORESIZE
    );
    l->SetBackgroundColour(::wxColor(b[L"background"].AsString()));

    this->SetPosition(l, b[L"position"]);
    this->SetFont(l, b[L"font"]);
    return l;
  }
  
  wxBitmap wxMMCSkin::GetBackground (::wxFrame *frame) {
    return wxMMCResource::Get(root[frame->GetTitle()][wxT("background")].AsString());
  }
  
  void wxMMCSkin::Setup (wxMMCFrame *frame) {
    ::wxString bg = root[frame->GetTitle()][wxT("background")].AsString();
    if (L"#"[0] == bg[0]) {
      frame->SetBackground(::wxColor(bg));
    }
    else if (!bg.IsNull()) {
      frame->SetBackground(this->GetBackground(frame));
    }
    if (root[frame->GetTitle()][L"font"].IsValid()) {
      this->SetFont(frame, root[frame->GetTitle()][L"font"]);
    }
  }
  
  ::wxPanel* wxMMCSkin::GetPanel (::wxFrame *frame, const wxString& panel) {
    ::wxJSONValue node = root[frame->GetTitle()][L"panels"][panel];
    ::wxPanel *res = new ::wxPanel(frame);
    this->SetPosition(res, node[L"position"]);
    this->SetFont(res, node[L"font"]);
    res->SetBackgroundColour(::wxColor(node[L"background"].AsString()));
    return res;
  }
  
  void wxMMCSkin::SetPosition (::wxWindow *wnd, ::wxJSONValue &p) {
    if (p.IsObject()) {
      ::wxSize size(0, 0);
      if (p[L"width"].IsInt() && p[L"height"].IsInt()) {
        size.Set(p[L"width"].AsInt(), p[L"height"].AsInt());
      }
      if (0 == size.x || 0 == size.y) {
        size = ::wxDefaultSize;
      }
      if (p[L"x"].IsInt() && p[L"y"].IsInt()) {
        wnd->SetPosition(::wxPoint(p[L"x"].AsInt(), p[L"y"].AsInt()));
      }
      wnd->SetSize(size);
    }
  }

  wxMMCCanPanelItem* wxMMCSkin::GetCanPanelItem (::wxFrame *frame, const wxString& item, wxWindowID id, const wxString& text) {
    ::wxJSONValue i = root[frame->GetTitle()][L"items"][item];
    ::wxJSONValue l = i[L"label"];
    ::wxBitmap bm = ::wxNullBitmap;
    ::wxString defText = L"",
               defSign = L"";
    if (i[L"icon"].IsString()) {
      bm = wxMMCResource::Get(i[L"icon"].AsString());
    }
    if (l[L"text"].IsValid()) {
      defText = l[L"text"].AsString();
    }
    if (l[L"sign"].IsString()) {
      defSign = l[L"sign"].AsString();
    }
    wxMMCCanPanelItem *cpi = new wxMMCCanPanelItem(frame, bm, defText);
    this->SetPosition(cpi, i[L"position"]);
    cpi->SetItemLabelOffset(::wxPoint(l[L"position"][L"x"].AsInt(), l[L"position"][L"y"].AsInt()));
    return cpi;
  }
  
  ::wxArrayString wxMMCSkin::GetFrameItemNames (::wxFrame *frame) {
    if (root[frame->GetTitle()][wxT("items")].IsValid()) {
      return root[frame->GetTitle()][wxT("items")].GetMemberNames();
    }
    return ::wxArrayString();
  }
  
  void wxMMCSkin::SetFont (::wxWindow *wnd, ::wxJSONValue &fnt) {
    wxFont f = wnd->GetFont();
    if (fnt[L"color"].IsString()) {
      wnd->SetForegroundColour(::wxColor(fnt[L"color"].AsString()));
    }
    if (fnt[wxT("size")].IsInt()) {
      f.SetPointSize(fnt[L"size"].AsInt());
    }
    if (fnt[wxT("face")].IsString()) {
      f.SetFaceName(fnt[L"face"].AsString());
    }
    wnd->SetFont(f);
  }

  wxMMCSkin::~wxMMCSkin () {
    wxMMCResource::FontsFree();
  }
}