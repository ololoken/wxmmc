#pragma once

#include <wx/wx.h>
#include "mmc_api.h"
#include "constants.h"

namespace mmc {

  class wxApiHandler : public ::wxThread {
    public:
      //must equal to mmc_api.h messages list!!!!
      typedef enum MMC_MESSAGE {
        UWM_RADIO_AUTOSEARCH_END = MMC_MSG_OFFSET,
        UWM_RADIO_AMS_END,
        UWM_RADIO_TUNED_OK,
        UWM_RADIO_NEW_FREQ,
        UWM_RADIO_BAND,
        UWM_RADIO_STEREO_UPDATE,
        UWM_RADIO_LODX_MOD,
        UWM_RADIO_PTY_SEARCH_END,
        UWM_RADIO_NEW_PS,
        UWM_RADIO_NEW_PTY,
        UWM_RADIO_TP_UPDATE,
        UWM_RADIO_TA_UPDATE,
        UWM_RADIO_RT_UPDATE,
        UWM_RADIO_AF_UPDATE,
        UWM_RADIO_EON_UPDATE,
        UWM_RADIO_PREFREQ_UPDATE,
        UWM_RADIO_LOC_SEN,
        UWM_RADIO_IC_MSG,
        UWM_RADIO_AF_FREQ,
        UWM_BT_DIAL_START,
        UWM_BT_NEW_CALL,
        UWM_AUD_VOL,
        UWM_AUD_MUTE,
        UWM_AUD_SOUND_READY,
        UMW_AUD_BASS,
        UWM_AUD_MID,
        UWM_AUD_TREBLE,
        UWM_AUD_BALANCE,
        UWM_AUD_FADER,
        UWM_AUD_LOUD,
        UWM_AUD_EQ,
        UWM_AUD_MIXVOL,
        UWM_AUD_SUBWOOF,
        UWM_AUD_MIXACT,
        UWM_AUD_LOUDC,
        UMW_AUD_BASSC,
        UWM_AUD_MIDC,
        UWM_AUD_TREBLEC,
        UMW_AUD_BASSQ,
        UWM_AUD_MIDQ,
        UWM_COMAND,
        UWM_KEY_MODE,
        UWM_KEY_PUSHENC,
        UWM_KEY,
        UWM_KEY_MUTE,
        UMW_KEY_VOL,
        UWM_KEY_RADIO,
        UWM_KEY_MEDIA,
        UWM_KEY_NAVI,
        UWM_KEY_PREV,
        UWM_KEY_NEXT,
        UWM_KEY_BT_ANS,
        UWM_KEY_BT_CANCEL,
        UWM_KEY_ENTER,
        UWM_KEY_ASPS,
        UWM_KEY_INFO,
        UWM_KEY_MENU,
        UWM_KEY_AUDIO,
        UWM_KEY_AF,
        UWM_SYS_POWER_UP,
        UWM_SYS_POWER_DOWN,
        UWM_SYS_INIT_END,
        UWM_SYS_USB,
        UWM_SYS_EEPROM,
        UWM_RDS_TIME_UPDATA,
        UWM_MCU_TIME_UPDATA,
        UWM_PAGE_UPDATA, 
        UWM_VID_CONTRAST,
        UWM_VID_BRIGHT,
        UWM_VID_COLOR,
        UWM_VID_LIGHTMODE,
        UWM_VID_BACKLIGHT
      } MMC_MESSAGE;
      
      typedef enum MMC_STATE {
        Current = -1,
        Radio = MMC_STATE_RADIO,
        Media = MMC_STATE_MEDIA,
        Bluetooth =  MMC_STATE_BT,
        BluetoothMedia = MMC_STATE_BTMEDIA,
        BlietoothRadio = MMC_STATE_BTRADIO
      } MMC_STATE;

    public:
      static wxApiHandler& Instance () {
        static wxApiHandler instance;
        return instance;
      };
      
      MMC_MESSAGE MMCMessage (UINT msg);
      static MMC_STATE State (MMC_STATE state);

      ~wxApiHandler (void);

    protected:
      wxApiHandler ();
      void operator= (wxApiHandler const&);
      virtual ExitCode Entry ();
      static LRESULT CALLBACK Receiver (HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
      
      void RegisterMessages ();
    
    protected:
      HWND m_HWND;
      UINT m_messages[100];
      size_t MMC_MESSAGES_COUNT;
  };

}
