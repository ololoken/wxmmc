#pragma once
#include <wx/wx.h>
#include "../player/core/common.h"

namespace mmc {
  class wxPlayer : public ::wxEvtHandler {
    protected:
      wxPlayer () {}
      void operator= (wxPlayer const&);
      
    protected:
      player *m_player;
      ::wxWindow *m_playerOutput;
      void UpdateSize ();

    protected:
      void OnOutputSize (::wxSizeEvent &e);
      
    public:
      static wxPlayer& Get () {
        static wxPlayer ins;
        return ins;
      }

      bool Init (::wxWindow* playerOutput);
      virtual void Play (bool play = true);
      virtual void Play (::wxString url);
      void Pause ();
      void DeInit ();
      
    protected:
      DECLARE_EVENT_TABLE()
  };

}