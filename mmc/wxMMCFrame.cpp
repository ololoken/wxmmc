#include "wxMMCFrame.h"
#include "wxMMCApp.h"

namespace mmc {

  BEGIN_EVENT_TABLE(wxMMCFrame, ::wxFrame)
    EVT_PAINT(wxMMCFrame::OnPaint)
    EVT_MMCCAN(wxID_ANY, wxMMCFrame::RelayEvent)
    EVT_MMCAPI(wxID_ANY, wxMMCFrame::RelayEvent)
    EVT_MMCBT(wxID_ANY, wxMMCFrame::RelayEvent)
  END_EVENT_TABLE()

  const wxSize wxMMCFrame::FullScreen = wxSize(800, 480);

  wxMMCFrame::wxMMCFrame (::wxWindow* parent, const wxString &title, const wxPoint &pos, const wxSize &size, long style)
  : ::wxFrame(parent, wxID_ANY, title, pos, size, style) {
    this->m_bgImgLoaded = false;
    if (size == FullScreen) {
      this->ShowFullScreen(true);
    }
    this->SetBackgroundStyle(::wxBG_STYLE_COLOUR);
    wxGetApp().Skin->Setup(this);
  }

  wxMMCFrame::~wxMMCFrame () {

  }
  
  void wxMMCFrame::SetBackground (wxColor color) {
    this->m_bgColor = color;
    this->SetBackgroundColour(this->m_bgColor);
  }
  
  void wxMMCFrame::SetBackground (wxBitmap bitmap) {
    this->m_bgImg = bitmap;
    this->m_bgImgLoaded = true;
  }

  void wxMMCFrame::DrawBackground (::wxDC *dc, const ::wxRect &rect) {
    if (this->m_bgImgLoaded) {
      dc->Blit(::wxPoint(0, 0), rect.GetSize(), &::wxMemoryDC(this->m_bgImg), rect.GetPosition());
    }
  }

  void wxMMCFrame::OnPaint (wxPaintEvent &e) {
    wxPaintDC paintDC(this);
    if (this->m_bgImgLoaded) {
      this->DrawBackground(&paintDC, this->GetRect());
    }
  }
  
  void wxMMCFrame::RelayEvent (::wxEvent &e) {
    const wxWindowList& list = this->GetChildren();
    for (wxWindowList::compatibility_iterator node = list.GetFirst(); node; node = node->GetNext()){
      ((wxWindow *)node->GetData())->ProcessEvent(e);
    }
  }

}