/*
* @desc driver for nf2301 bluetooth module
*/

#pragma once

#include <windows.h>

#ifdef _DEBUG
#define NFORE_BT_PORT_NAME _T("COM2:")
#define NFORE_BT_PORT_BRATE CBR_115200
#else
#define NFORE_BT_PORT_NAME _T("COM2:")
#define NFORE_BT_PORT_BRATE CBR_115200
#endif

#ifdef BT_NFORE_EXPORTS
#define NFORE_BT_API __declspec(dllexport)
#else
#define NFORE_BT_API __declspec(dllimport)
#endif

#define UWM_NFORE_BT_CHANGE_CONN_STATE             (WM_USER+1)  // 97
#define UWM_NFORE_BT_HFP_STATE                     (WM_USER+2)  // 84
#define UWM_NFORE_BT_CHANGE_SOUND_STREAM           (WM_USER+3)  // 96

#define UWM_NFORE_BT_DETECTED_CALL_NUMBER          (WM_USER+5)  // 89 | 93
#define UWM_NFORE_BT_RSSI                          (WM_USER+6)  // 8D
#define UWM_NFORE_BT_BATTERY_CHG                   (WM_USER+7)  // 8F
#define UWM_NFORE_BT_CHANGE_OUT_MUTE_STATE         (WM_USER+8)  // C7
#define UWM_NFORE_BT_RESET_START                   (WM_USER+9)  // 80
#define UWM_NFORE_BT_RESET_DONE                     (WM_USER+10) // 81
#define UWM_NFORE_BT_FINISH_READ_DEVICES           (WM_USER+11) // B9
#define UWM_NFORE_BT_DISCONNECT_ACTION             (WM_USER+12) // BB

#define UWM_NFORE_BT_SET_VISIBILITY                (WM_USER+24) // 17
#define UWM_NFORE_BT_DEVICE_FOUND                  (WM_USER+25) // 1C
#define UWM_NFORE_BT_DEVICE_INFO                   (WM_USER+26) // 1E
#define UWM_NFORE_BT_SET_PIN_CODE                  (WM_USER+27) // 20
#define UWM_NFORE_BT_REQ_PIN                       (WM_USER+28) // 21
#define UWM_NFORE_BT_PAIR_DEVICE                   (WM_USER+29) // 22
#define UWM_NFORE_BT_PAIR_DEVICE_LIST              (WM_USER+30) // 23
#define UWM_NFORE_BT_PAIR_DEVICE_DELETE            (WM_USER+31) // 24
#define UWM_NFORE_BT_PAIR_DEVICE_CONNECT           (WM_USER+32) // 25
#define UWM_NFORE_BT_PAIR_DEVICE_DISCONNECT        (WM_USER+33) // 26

#define UWM_NFORE_BT_CALL_ANSWER                   (WM_USER+35) // 29
#define UWM_NFORE_BT_CALL_REJECT                   (WM_USER+36) // 2A
#define UWM_NFORE_BT_DEV_SEAECH_STOP               (WM_USER+37) // 2B
#define UWM_NFORE_BT_CALL_DIAL                     (WM_USER+38) // 2C
#define UWM_NFORE_BT_CALL_REDIAL                   (WM_USER+39) // 2E
                                                         // 2F VoiceDial?
#define UWM_NFORE_BT_VOICE_OUT_PATH                (WM_USER+41) // 30
#define UWM_NFORE_BT_DTMF                          (WM_USER+42) // 31
#define UWM_NFORE_BT_SET_VOLUME                    (WM_USER+43) // 32
#define UWM_NFORE_BT_SET_MIC_VOLUME                (WM_USER+44) // 33
#define UWM_NFORE_BT_AVRCP_CMD                     (WM_USER+45) // 3D
#define UWM_NFORE_BT_MUTE                          (WM_USER+46) // 4F
#define UWM_NFORE_BT_RINGTONE_LEVEL                (WM_USER+47) // 53
#define UWM_NFORE_BT_MIC_INPUT                     (WM_USER+48) // 59
#define UWM_NFORE_BT_MIC_GAIN                      (WM_USER+49) // 5A

#define UWM_NFORE_BT_CMD_ERROR                     (WM_USER+50)
#define UWM_NFORE_BT_PORT_EMULATOR                 (WM_USER+51)
#define UWM_NFORE_PORT_ERROR                       (WM_USER+52)
#define UWM_NFORE_BT_RUNNING_STATE                 (WM_USER+53)
#define UWM_NFORE_BT_SEARCH_DEVICES_END            (WM_USER+54)
#define UWM_NFORE_BT_MODULE_STATUS                 (WM_USER+55)
#define UWM_NFORE_BT_PAIR_DEVICE_LIST_END          (WM_USER+56)
#define UWM_NFORE_BT_PAIR_DEVICE_LIST_EMPTY        (WM_USER+57)
#define UWM_NFORE_BT_PAIR_DEVICE_DONE              (WM_USER+58)
#define UWM_NFORE_BT_CALL_NUMBER                   (WM_USER+59)
#define UWM_NFORE_BT_A2DP_STATE                    (WM_USER+60)
#define UWM_NFORE_BT_AG_STATE                      (WM_USER+61)
#define UWM_NFORE_BT_AVRCP_STATE                   (WM_USER+62)
#define UWM_NFORE_BT_READ_REMOTE_INFO              (WM_USER+63)
#define UWM_NFORE_BT_MUTE_STATE                    (WM_USER+64)
#define UWM_NFORE_BT_PAIR_DEVICE_INFO              (WM_USER+65)
#define UWM_NFORE_BT_SEARCH_DEVICES_CONTINUE       (WM_USER+66)

#define NFORE_BT_MIN_BT_VOL                   0
#define NFORE_BT_MAX_BT_VOL                   15

#define NFORE_BT_ERR_NONE 0
#define NFORE_BT_ERR_INITED -1001
#define NFORE_BT_ERR_PORT -1002
#define NFORE_BT_ERR_THREAD -1003
#define NFORE_BT_ERR_BUSY -1004
#define NFORE_BT_ERR_DEV_BUSY 0x8B

#define NFORE_BT_CMD_GET_RAW_BUFFER         0x00
#define NFORE_BT_CMD_MODULE_RESET           0x11
#define NFORE_BT_CMD_MODULE_INFO            0x12
#define NFORE_BT_CMD_MODULE_STATUS          0x13
#define NFORE_BT_CMD_SET_LOCAL_NAME         0x16
#define NFORE_BT_CMD_SET_VISIBILITY         0x17
#define NFORE_BT_CMD_SEARCH_DEVICES         0x1C
#define NFORE_BT_CMD_GET_DEVICE_INFO        0x1E
#define NFORE_BT_CMD_SET_LOCAL_PIN          0x20
#define NFORE_BT_CMD_REQUIRE_PIN            0x21

#define NFORE_BT_CMD_PAIR_DEVICE            0x22
#define NFORE_BT_CMD_PAIR_DEVICE_LIST       0x23
#define NFORE_BT_CMD_PAIR_DEVICE_LIST_END   0xB9
#define NFORE_BT_CMD_PAIR_DEVICE_DELETE     0x24
#define NFORE_BT_CMD_PAIR_DEVICE_CONNECT    0x25
#define NFORE_BT_CMD_PAIR_DEVICE_DISCONNECT 0x26
#define NFORE_BT_CMD_SEARCH_DEVICE_STOP     0x2A
#define NFORE_BT_CMD_PAIR_DEVICE_INFO       0x82
#define NFORE_BT_CMD_PAIR_DEVICE_DONE       0x83

#define NFORE_BT_CMD_CALL_ANSWER            0x29
#define NFORE_BT_CMD_CALL_REJECT            0x2A
#define NFORE_BT_CMD_CALL_TERMINATE         0x2B
#define NFORE_BT_CMD_CALL_DIAL              0x2C
#define NFORE_BT_CMD_CALL_REDIAL            0x2E
#define NFORE_BT_CMD_CALL_VOICEDIAL         0x2F
#define NFORE_BT_CMD_WAVE_OUT               0x30
#define NFORE_BT_CMD_DTMF                   0x31//tone dial
#define NFORE_BT_CMD_SET_VOLUME             0x32
#define NFORE_BT_CMD_SET_MIC_VOLUME         0x33

#define NFORE_BT_CMD_SPP                    0x4A//msg_len payload CheckSum_8_Xor
#define NFORE_BT_CMD_MUTE                   0x4F
#define NFORE_BT_CMD_PB_DOWNLOAD            0x54

#define NFORE_BT_CMD_RESET_START            0x80
#define NFORE_BT_CMD_RESET_DONE             0x81

#define NFORE_BT_CMD_MODE_3G                0x62

#define NFORE_BT_CMD_HFP_STATE              0x84
#define NFORE_BT_CMD_WAVE_OUT_CHANGE        0x85
#define NFORE_BT_CMD_CALL_PHONE_NUM_IN      0x89
#define NFORE_BT_CMD_RSSI                   0x8D
#define NFORE_BT_CMD_BATTERY_CHG            0x8F
#define NFORE_BT_CMD_CALL_PHONE_NUM_OUT     0x93
#define NFORE_BT_CMD_A2DP_STATE             0x95
#define NFORE_BT_CMD_AG_STATE               0x96
#define NFORE_BT_CMD_AVRCP_STATE            0x97
#define NFORE_BT_CMD_REMOTE_INFO            0xAC
#define NFORE_BT_CMD_DISCONNECT             0xBB
#define NFORE_BT_CMD_MUTE_STATE             0xC7

#define NFORE_BT_CMD_AVRCP_CMD              0x3D

#define BT_MAC_LEN 0x6
#define BT_COD_LEN 0x4
#define BT_INQUERY_COD_LEN 0x3
#define BT_NFORE_DEVICE_ALL 0x255

#define BT_MAX_DATA_LEN 0xFF
#define BT_MAX_DEVICES 9
#define BT_MAX_CALLS 5
#define BT_MAX_CMD_QUE 100

#pragma region enums

typedef enum {//AT#RPT
  HFP_INITIALIZING = 0,
  HFP_READY = 1,
  HFP_CONNECTING = 2,
  HFP_CONNECTED = 3,
  HFP_OUTGOIND = 4,
  HFP_INCOMING = 5,
  HFP_ACTIVE = 6
} BT_HFP_STATE;

typedef enum {
  BT_VIS_NO = 0,
  BT_VIS_PAIRED = 2,
  BT_VIS_ALL = 3
} BT_VISIBILITY;

typedef enum {//AT#QAS
  AVRCP_INITIALIZING = 0,
  AVRCP_READY = 1,
  AVRCP_CONNECTING = 2,
  ACRCP_CONNECTED = 3
} BT_AVRCP_STATE;

#pragma endregion enums

#pragma region structs
typedef BYTE BT_MAC[BT_MAC_LEN], *LPBT_MAC;
typedef BYTE BT_CoD[BT_COD_LEN], *LPBT_CoD;
typedef BYTE BT_INQUERY_CoD[BT_INQUERY_COD_LEN], *LPBT_INQUERY_CoD;

typedef struct BT_PIN {//ansi string
  CHAR data[BT_MAX_DATA_LEN];
  BYTE length;
} BT_PIN, *LPBT_PIN;
typedef struct BT_NAME {//utf16-be string
  CHAR data[BT_MAX_DATA_LEN];
  BYTE length;
} BT_NAME, *LPBT_NAME;
typedef struct BT_PHONE_NUM {//ansi string
  CHAR data[BT_MAX_DATA_LEN];
  BYTE length;
} BT_PHONE_NUM, *LPBT_PHONE_NUM;

typedef struct NFORE_CMD {
  BYTE code;
  BYTE length;
  BYTE payload[BT_MAX_DATA_LEN];
  BYTE waitCode;
} NFORE_CMD, *LPNFORE_CMD;

typedef struct BT_DEVICE {
  BT_MAC MAC;
  BT_CoD CoD;
  BT_NAME name;
  BYTE index;
  BOOL connected;
  BOOL paired;
  BOOL refreshed;
} BT_DEVICE, *LPBT_DEVICE;

typedef struct BT_DEVICE_LOCAL_STATE {
  BYTE power;
  BYTE visibility;
  BYTE stateX;
  BYTE outPath;
  BYTE volumeSpk;
  BYTE volumeMic;
  BYTE hfpState;
  BYTE a2dpState;
  BYTE avrcpState;
  BYTE stateXX;
  BYTE stateXXX;
  BYTE stateXXXX;
} BT_DEVICE_LOCAL_STATE, *LPBT_DEVICE_LOCAL_STATE;

typedef struct BT_DEVICE_LOCAL_INFO {
  struct BT_DEVICE;
  BYTE fwVersion[4];
  BT_PIN pin;
  struct BT_DEVICE_LOCAL_STATE state;
} BT_DEVICE_LOCAL_INFO, *LPBT_DEVICE_LOCAL_INFO;

typedef struct BT_CALL {
  BT_PHONE_NUM num;
  BT_HFP_STATE state;
  BYTE idx;
} BT_CALL, *LPBT_CALL;

#pragma endregion structs

typedef BT_DEVICE BT_DEV_LIST[BT_MAX_DEVICES];
typedef BT_DEV_LIST *LPBT_DEV_LIST;
typedef BT_CALL BT_CALLS[BT_MAX_CALLS];
typedef BT_CALLS *LPBT_CALLS;

#ifdef __cplusplus
extern "C" {
#endif
NFORE_BT_API int BT_NFORE_Open (HWND hWnd);//open bt port
NFORE_BT_API int BT_NFORE_Close ();//close bt port
NFORE_BT_API int BT_NFORE_Reset ();//reset bt
NFORE_BT_API int BT_NFORE_Init ();//fetch module info, status and paired devices list (init internal state)
NFORE_BT_API int BT_NFORE_LogStart (const char* fileName);//enable logging
NFORE_BT_API int BT_NFORE_LogStop ();//disable logging

NFORE_BT_API int BT_NFORE_SendCmd (LPNFORE_CMD);
NFORE_BT_API int BT_NFORE_SetVisible (BT_VISIBILITY v);//change dev visibility
NFORE_BT_API int BT_NFORE_PairList ();//refresh paired devices list
NFORE_BT_API int BT_NFORE_DevSearchStart (LPBT_INQUERY_CoD cod);//device search, other commands will fail during active search
NFORE_BT_API int BT_NFORE_DevSearchStop ();
NFORE_BT_API int BT_NFORE_Connect (LPBT_MAC mac, BYTE index);//connect device by MAC
NFORE_BT_API int BT_NFORE_Pair (LPBT_MAC mac);//pair device
NFORE_BT_API int BT_NFORE_SetPin (LPBT_PIN pin);
NFORE_BT_API int BT_NFORE_SetPinRq (BOOL);

NFORE_BT_API LPBT_DEV_LIST BT_NFORE_GetPaired ();
NFORE_BT_API LPBT_DEV_LIST BT_NFORE_GetFound ();

NFORE_BT_API int BT_NFORE_SendData (BYTE *buffer, size_t length);

const BT_INQUERY_CoD NFORE_BT_INQUERY_MEDIA = {0x00, 0x1C, 0x0A};
const BT_INQUERY_CoD NFORE_BT_INQUERY_NETWORK = {0x00, 0x0A, 0x0C};

#ifdef __cplusplus
}
#endif