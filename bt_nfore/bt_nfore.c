#include "bt_nfore.h"

HANDLE 
  g_hPort = NULL,
  g_logFile = NULL,
  g_eventNewCommand = NULL,
  g_eventCanContinue = NULL,
  DllPortWriteThread = NULL,
  DllPortReadThread = NULL;

HWND g_HWND_dest = NULL;

BOOL g_emulator = 0;

BT_DEVICE_LOCAL_INFO g_devLocalInfo = {0};
LPBT_DEVICE_LOCAL_INFO g_devLocalInfoPtr = &g_devLocalInfo;

BT_DEV_LIST g_devFound = {0};
size_t g_devFoundLen = 0;

BT_DEV_LIST g_devPaired = {0};

BT_DEVICE g_devPairing = {0};

LPBT_DEVICE g_devCurrentPair = NULL;

BT_CALLS g_calls = {0};

NFORE_CMD g_cmdQue[BT_MAX_CMD_QUE] = {0};
size_t g_cmdQueInPos = 0,
       g_cmdQueOutPos = 0;
       
LPNFORE_CMD g_currentCommand = {0};

CRITICAL_SECTION g_portWriteOperation, g_logOperation, g_cmdQueOperation;

BOOL g_searchInfinite = FALSE;

void InitInstance ();
void CloseInstance ();
BOOL OpenPort ();
BOOL PortWrite (BYTE* payload, size_t length);
int SendCmd (LPNFORE_CMD cmd);//send raw cmd
DWORD WINAPI PortReadThreadProc (LPVOID lpParameter);
DWORD WINAPI PortWriteThreadProc (LPVOID lpParameter);
BOOL ProcessCommand (const LPNFORE_CMD cmd);
LPBT_DEVICE GetDevByMAC (BT_DEVICE *list, size_t len, const BT_MAC MAC);
void LogCmd(LPNFORE_CMD cmd, LPCSTR prefix);
__inline BOOL PostBack (UINT msg, WPARAM w, LPARAM l);
void FetchDeviceInfo (LPBT_MAC mac);
void SearchContinue ();


BOOL OpenPort () {
  LPCWSTR comname;
  DWORD dwerrors;
  BOOL fSuccess;
  COMMTIMEOUTS PortTimeouts;
  COMSTAT comstat;
  COMMPROP comprop;
  DCB dcb;
  float fOneCharTime;

  comname = NFORE_BT_PORT_NAME;
  if (g_hPort) {
    CloseHandle(g_hPort);
    g_hPort = NULL;
  }
  if(!g_hPort) {
    g_hPort = CreateFile(comname, 
                         GENERIC_READ|GENERIC_WRITE, 
                         FILE_SHARE_READ|FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);
    if (g_hPort == INVALID_HANDLE_VALUE) {
      dwerrors = GetLastError();
      PostBack(UWM_NFORE_PORT_ERROR, (WPARAM)dwerrors, 0);
      g_hPort = NULL;
      return FALSE;
    }
  }
  dcb.DCBlength = sizeof(DCB);
  fSuccess = GetCommProperties(g_hPort, &comprop);
  if(!fSuccess) {
    CloseHandle(g_hPort);
    g_hPort = NULL;
    return FALSE;
  }

  GetCommState(g_hPort, &dcb);
  dcb.fDummy2 = (dcb.fDummy2 & 0xFFFFDCD3 | 0x1010) & 0xFFFFF7FD;
  dcb.BaudRate = NFORE_BT_PORT_BRATE;     //  baud rate
  dcb.Parity = NOPARITY;      //  parity bit
  dcb.ByteSize = 8;             //  data size, xmit and rcv
  dcb.StopBits = ONESTOPBIT;    //  stop bit
  dcb.XoffLim = 100;
  dcb.XonLim = 100;
  dcb.XonChar = 0x11;
  dcb.XoffChar = 0x13;
  fSuccess = SetCommState(g_hPort, &dcb);
  
  if (!fSuccess) {//emulator
    g_emulator = 1;
  }
  PostBack(UWM_NFORE_BT_PORT_EMULATOR, (WPARAM)g_emulator, 0);
  if (comprop.dwMaxRxQueue == 0) {
    comprop.dwMaxRxQueue = 4096;
  }
  if (comprop.dwMaxTxQueue == 0) {
    comprop.dwMaxTxQueue = 4096;
  }
  SetupComm(g_hPort, comprop.dwMaxRxQueue, comprop.dwMaxTxQueue);

  fOneCharTime = 1/(float)dcb.BaudRate*1000000;

  PortTimeouts.ReadIntervalTimeout = (DWORD)ceil(fOneCharTime*3);
  PortTimeouts.ReadTotalTimeoutMultiplier = (DWORD)ceil(fOneCharTime*2);
  PortTimeouts.ReadTotalTimeoutConstant = (DWORD)ceil(fOneCharTime*3);
  PortTimeouts.WriteTotalTimeoutMultiplier = PortTimeouts.ReadTotalTimeoutMultiplier;
  PortTimeouts.WriteTotalTimeoutConstant = PortTimeouts.ReadTotalTimeoutConstant;
  SetCommTimeouts(g_hPort, &PortTimeouts);

  ClearCommBreak(g_hPort);
  PurgeComm(g_hPort, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR); // ������� �������
  ClearCommError(g_hPort, &dwerrors, &comstat);

  return TRUE;
}


void ClosePort () {
  if((g_hPort != 0) && (g_hPort != INVALID_HANDLE_VALUE)) {
    CloseHandle(g_hPort);
    g_hPort = NULL;
  }
}

DWORD WINAPI PortReadThreadProc (LPVOID lpParameter) {
  static NFORE_CMD cmd = {0};
  static DWORD dwCommModemStatus = 0,
               dwBytesTransferred = 0;
  PostBack(UWM_NFORE_BT_RUNNING_STATE, (WPARAM)1, 0);
  SetCommMask(g_hPort, EV_RXCHAR|EV_CTS|EV_DSR|EV_RLSD|EV_RING);
  while (g_hPort && INVALID_HANDLE_VALUE != g_hPort) {
    WaitCommEvent(g_hPort, &dwCommModemStatus, 0);
    SetCommMask(g_hPort, EV_RXCHAR|EV_CTS|EV_DSR|EV_RING);
    if (dwCommModemStatus & EV_RXCHAR) {
      ReadFile(g_hPort, &cmd.code, 1, &dwBytesTransferred, 0);
      if (cmd.code > 0) {
        ReadFile(g_hPort, &cmd.length, 1, &dwBytesTransferred, 0);
        ReadFile(g_hPort, &cmd.payload[0], cmd.length, &dwBytesTransferred, 0);
        LogCmd(&cmd, "rx");
        ProcessCommand(&cmd);
        memset(&cmd, 0x00, sizeof(NFORE_CMD));
      }
    }
  }
  PostBack(UWM_NFORE_BT_RUNNING_STATE, (WPARAM)0, 0);
  ExitThread(0);
  return 0;
}

DWORD WINAPI PortWriteThreadProc (LPVOID lpParameter) {
  static BYTE buff[300];
  static LPNFORE_CMD cmd;
  do {
    if (g_cmdQueOutPos == g_cmdQueInPos) {//wait new command if que is empty
      ResetEvent(g_eventNewCommand);
      WaitForSingleObject(g_eventNewCommand, INFINITE);
    }
    //get cmd
    EnterCriticalSection(&g_cmdQueOperation);
    cmd = &g_cmdQue[g_cmdQueOutPos];
    g_currentCommand = &g_cmdQue[g_cmdQueOutPos];
    g_cmdQueOutPos = (++g_cmdQueOutPos)%BT_MAX_CMD_QUE;
    LeaveCriticalSection(&g_cmdQueOperation);
    //log und write cmd to port
    LogCmd(cmd, "tx");
    buff[0] = cmd->code;
    buff[1] = cmd->length;
    memcpy(&buff[2], &cmd->payload[0], cmd->length);
    PortWrite(buff, cmd->length+2);
    ResetEvent(g_eventCanContinue);
    WaitForSingleObject(g_eventCanContinue, 5000);//5s timeout for waiting resp from sent command
  } while (g_hPort);

  return 0;
}

__inline BOOL PostBack (UINT msg, WPARAM w, LPARAM l) {
  return PostMessage(g_HWND_dest, msg, w, l);
}

BOOL ProcessCommand (const LPNFORE_CMD cmd) {
  size_t offs = 0;
  BYTE status = 0;
  if (cmd->length) {
    status = cmd->payload[0];
    offs += 1;
  }
  if (NULL != g_currentCommand) {
    if (NFORE_BT_CMD_SEARCH_DEVICES != g_currentCommand->code) {
      if (0 == g_currentCommand->waitCode || g_currentCommand->waitCode == cmd->code) {
        g_currentCommand = NULL;
        SetEvent(g_eventCanContinue);
      }
    }
  }
  switch (cmd->code) {
    case NFORE_BT_CMD_MODULE_INFO: {
      if (0x0 != status) {
        return PostBack(UWM_NFORE_BT_CMD_ERROR, NFORE_BT_CMD_MODULE_INFO, status);
      }
      memset(&g_devLocalInfo, 0x00, sizeof(BT_DEVICE_LOCAL_INFO));
      memcpy(&g_devLocalInfo.MAC[0], &cmd->payload[offs], BT_MAC_LEN);
      offs += BT_MAC_LEN;
      memcpy(&g_devLocalInfo.CoD[0], &cmd->payload[offs], BT_COD_LEN);
      offs += BT_COD_LEN;
      memcpy(&g_devLocalInfo.CoD[0], &cmd->payload[offs], 4);
      offs += 4;
      g_devLocalInfo.pin.length = cmd->payload[offs];
      offs += 1;
      memcpy(&g_devLocalInfo.pin.data[0], &cmd->payload[offs], g_devLocalInfo.pin.length);
      offs += g_devLocalInfo.pin.length;
      g_devLocalInfo.name.length = cmd->payload[offs];
      offs += 1;
      memcpy(&g_devLocalInfo.name.data[0], &cmd->payload[offs], g_devLocalInfo.name.length);
      return PostBack(UWM_NFORE_BT_MODULE_STATUS, 0, (LPARAM)g_devLocalInfoPtr);
    }
    case NFORE_BT_CMD_MODULE_STATUS: {
      if (0 != status) {
        return PostBack(UWM_NFORE_BT_CMD_ERROR, NFORE_BT_CMD_MODULE_STATUS, status);
      }
      memcpy(&g_devLocalInfo.state, &cmd->payload[offs], sizeof(BT_DEVICE_LOCAL_STATE));
      return PostBack(UWM_NFORE_BT_MODULE_STATUS, 0, (LPARAM)g_devLocalInfoPtr);
    }
    case NFORE_BT_CMD_SET_LOCAL_NAME: {
      if (0 != status) {
        return PostBack(UWM_NFORE_BT_CMD_ERROR, NFORE_BT_CMD_SET_LOCAL_NAME, status);
      }
      g_devLocalInfo.name.length = cmd->length;
      memcpy(&g_devLocalInfo.name.data[0], &cmd->payload[0], g_devLocalInfo.name.length);
      return PostBack(UWM_NFORE_BT_MODULE_STATUS, 0, (LPARAM)g_devLocalInfoPtr);
    }
    case NFORE_BT_CMD_SET_VISIBILITY: {
      g_devLocalInfo.state.visibility = status;
      PostBack(UWM_NFORE_BT_SET_VISIBILITY, 0, (LPARAM)g_devLocalInfoPtr);
      return PostBack(UWM_NFORE_BT_MODULE_STATUS, 0, (LPARAM)g_devLocalInfoPtr);
    };
    case NFORE_BT_CMD_SEARCH_DEVICES: {
      if (0 == status) {
        BT_DEVICE dev;
        LPBT_DEVICE devStored;
        memcpy(&dev.MAC[0], &cmd->payload[offs], BT_MAC_LEN);
        offs += BT_MAC_LEN;
        memcpy(&dev.CoD[0], &cmd->payload[0], BT_COD_LEN);
        devStored = GetDevByMAC(g_devFound, g_devFoundLen, dev.MAC);
        if (NULL == devStored) {
          g_devFoundLen++;
          if (g_devFoundLen > BT_MAX_DEVICES) {
            g_devFoundLen = 1;
          }
          dev.index = g_devFoundLen;
          g_devFound[g_devFoundLen] = dev;
          devStored = &g_devFound[g_devFoundLen];
          FetchDeviceInfo(dev.MAC);//que fetch name of found device command
        }
        devStored->refreshed = TRUE;
        return PostBack(UWM_NFORE_BT_DEVICE_FOUND, g_devFoundLen, (LPARAM)devStored);
      }
      else if (1 == status) {//device search not fount/end
        //erase not refreshed devices
        size_t i = 1;
        do {
          if (g_devFound[i].refreshed) {
            ++i;
          }
          else {
            --g_devFoundLen;
            memmove(&g_devFound[i], &g_devFound[i+1], sizeof(g_devFound));
          }
        } while (i < g_devFoundLen);
        SetEvent(g_eventCanContinue);//run fetching of names
        SearchContinue();//que next search loop
        return PostBack(UWM_NFORE_BT_SEARCH_DEVICES_END, 0, 0);
      }
      else {//unknown status
        return PostBack(UWM_NFORE_BT_CMD_ERROR, NFORE_BT_CMD_SEARCH_DEVICES, status);
      }
    }
    case NFORE_BT_CMD_GET_DEVICE_INFO: {
      BT_MAC mac = {0};
      LPBT_DEVICE dev = NULL;
      if (0 != status) {
        return PostBack(UWM_NFORE_BT_CMD_ERROR, NFORE_BT_CMD_GET_DEVICE_INFO, status);
      }
      memcpy(&mac, &cmd->payload[offs], BT_MAC_LEN);
      offs += BT_MAC_LEN;
      dev = GetDevByMAC(g_devFound, g_devFoundLen, mac);
      if (NULL == dev) {
        return PostBack(UWM_NFORE_BT_CMD_ERROR, NFORE_BT_CMD_GET_DEVICE_INFO, -1);
      }
      dev->name.length = cmd->payload[offs];
      offs += 1;
      memcpy(&dev->name.data[0], &cmd->payload[offs], dev->name.length);
      return PostBack(UWM_NFORE_BT_DEVICE_INFO, sizeof(BT_DEVICE), (LPARAM)dev);
    }
    case NFORE_BT_CMD_SET_LOCAL_PIN: {
      if (0 != status) {
        return PostBack(UWM_NFORE_BT_CMD_ERROR, NFORE_BT_CMD_SET_LOCAL_PIN, status);
      }
      g_devLocalInfo.pin.length = cmd->length;
      memcpy(&g_devLocalInfo.pin.data[0], &cmd->payload[offs], g_devLocalInfo.pin.length);
      return PostBack(UWM_NFORE_BT_SET_PIN_CODE, 0, (LPARAM)g_devLocalInfoPtr);
    }
    case NFORE_BT_CMD_REQUIRE_PIN: {
      return PostBack(UWM_NFORE_BT_REQ_PIN, NFORE_BT_CMD_REQUIRE_PIN, status);
    }
    case NFORE_BT_CMD_PAIR_DEVICE: {
      return PostBack(UWM_NFORE_BT_PAIR_DEVICE, 0, status);
    }
    case NFORE_BT_CMD_PAIR_DEVICE_LIST: {
      if (0 == status) {
        BYTE index = cmd->payload[offs];
        LPBT_DEVICE dev = &g_devPaired[index];
        offs += 1;
        dev->index = index;
        memcpy(&dev->MAC[0], &cmd->payload[offs], BT_MAC_LEN);
        offs += BT_MAC_LEN;
        dev->name.length = cmd->payload[offs];
        offs += 1;
        memcpy(&dev->name.data[0], &cmd->payload[offs], dev->name.length);
        dev->paired = TRUE;
        return PostBack(UWM_NFORE_BT_PAIR_DEVICE_LIST, index, (LPARAM)dev);
      }
      else if (0x89 == status) {
        return PostBack(UWM_NFORE_BT_PAIR_DEVICE_LIST_EMPTY, 0, status);
      }
      else {
        return PostBack(UWM_NFORE_BT_CMD_ERROR, NFORE_BT_CMD_PAIR_DEVICE_LIST, status);
      }
    }
    case NFORE_BT_CMD_PAIR_DEVICE_LIST_END: {
      return PostBack(UWM_NFORE_BT_PAIR_DEVICE_LIST_END, 0, (LPARAM)&g_devPaired);
    }
    case NFORE_BT_CMD_PAIR_DEVICE_DELETE: {
      BYTE index = cmd->payload[offs];
      if (0xFF == index) {
        memset(&g_devPaired, 0x00, sizeof(g_devPaired));
      }
      else {
        memset(&g_devPaired[index], 0x00, sizeof(g_devPaired[index]));
      }
      return PostBack(UWM_NFORE_BT_PAIR_DEVICE_DELETE, index, status);
    }
    case NFORE_BT_CMD_PAIR_DEVICE_CONNECT: {
      BYTE index = cmd->payload[offs];
      LPBT_DEVICE dev = &g_devPaired[index];
      if (status != 0) {
        return PostBack(UWM_NFORE_BT_CMD_ERROR, NFORE_BT_CMD_PAIR_DEVICE_CONNECT, status);
      }
      index = cmd->payload[offs];
      offs += 1;
      memcpy(&dev->MAC[0], &cmd->payload[offs], BT_MAC_LEN);
      offs += BT_MAC_LEN;
      dev->connected = TRUE;
      return PostBack(UWM_NFORE_BT_PAIR_DEVICE_CONNECT, index, (LPARAM)dev);
    }
    case NFORE_BT_CMD_PAIR_DEVICE_DISCONNECT: {
      BYTE index = cmd->payload[offs];
      if (status != 0) {
        return PostBack(UWM_NFORE_BT_CMD_ERROR, NFORE_BT_CMD_PAIR_DEVICE_DISCONNECT, status);
      }
      g_devPaired[index].connected = FALSE;
      return PostBack(UWM_NFORE_BT_PAIR_DEVICE_DISCONNECT, index, status);
    }
    case NFORE_BT_CMD_CALL_ANSWER: {
      return PostBack(UWM_NFORE_BT_CALL_ANSWER, 0, status);
    }
    case NFORE_BT_CMD_CALL_REJECT: {
      return PostBack(UWM_NFORE_BT_CALL_REJECT, 0, status);
    }
    case NFORE_BT_CMD_CALL_DIAL: {
      return PostBack(UWM_NFORE_BT_CALL_DIAL, 0, status);
    }
    case NFORE_BT_CMD_CALL_REDIAL: {
      return PostBack(UWM_NFORE_BT_CALL_REDIAL, 0, status);
    }
    case NFORE_BT_CMD_WAVE_OUT: {
      return PostBack(UWM_NFORE_BT_VOICE_OUT_PATH, 0, status);
    }
    case NFORE_BT_CMD_DTMF: {
      return PostBack(UWM_NFORE_BT_DTMF, 0, status);
    }
    case NFORE_BT_CMD_SET_VOLUME: {
      if (0 != status) {
        return PostBack(UWM_NFORE_BT_CMD_ERROR, NFORE_BT_CMD_SET_VOLUME, status);
      }
      return PostBack(UWM_NFORE_BT_SET_VOLUME, cmd->payload[1], status);
    }
    case NFORE_BT_CMD_SET_MIC_VOLUME: {
      if (0 != status) {
        return PostBack(UWM_NFORE_BT_CMD_ERROR, NFORE_BT_CMD_SET_MIC_VOLUME, status);
      }
      return PostBack(UWM_NFORE_BT_SET_MIC_VOLUME, 0, status);
    }
    case NFORE_BT_CMD_AVRCP_CMD: {
      return PostBack(UWM_NFORE_BT_AVRCP_CMD, cmd->payload[offs], status);
    }
    case NFORE_BT_CMD_MUTE: {
      return PostBack(UWM_NFORE_BT_MUTE, 0, status);
    }
    case NFORE_BT_CMD_PB_DOWNLOAD: {
      return FALSE;
    }
    case NFORE_BT_CMD_RESET_START: {
      return PostBack(UWM_NFORE_BT_RESET_START, 0, status);
    }
    case NFORE_BT_CMD_RESET_DONE: {
      return PostBack(UWM_NFORE_BT_RESET_DONE, 0, status);
    }
    case NFORE_BT_CMD_PAIR_DEVICE_INFO: {
      memset(&g_devPairing, 0x00, sizeof(g_devPairing));
      offs = 0; //no status byte here
      memcpy(&g_devPairing.MAC[0], &cmd->payload[offs], BT_MAC_LEN);
      offs += BT_MAC_LEN;
      g_devPairing.name.length = cmd->payload[offs];
      offs += 1;
      memcpy(&g_devPairing.name.data[0], &cmd->payload[offs], g_devPairing.name.length);
      return PostBack(UWM_NFORE_BT_PAIR_DEVICE_INFO, 0, (LPARAM)((LPBT_DEVICE)&g_devPairing));
    }
    case NFORE_BT_CMD_PAIR_DEVICE_DONE: {
      BYTE index = cmd->payload[offs];
      LPBT_DEVICE dev = &g_devPaired[index];
      offs += 1;
      if (0 != status) {
        return PostBack(UWM_NFORE_BT_CMD_ERROR, NFORE_BT_CMD_PAIR_DEVICE_DONE, status);
      }
      dev->index = index;
      memcpy(&dev->MAC[0], &cmd->payload[offs], BT_MAC_LEN);
      memcpy(&dev->name, &g_devPairing.name, sizeof(BT_NAME));
      dev->paired = TRUE;
      return PostBack(UWM_NFORE_BT_PAIR_DEVICE_DONE, index, (LPARAM)dev);
    }
    case NFORE_BT_CMD_HFP_STATE: {
      BYTE index = cmd->payload[offs];
      LPBT_CALL call = &g_calls[index];
      offs += 1;
      switch (status) {
        case HFP_INITIALIZING:
        case HFP_READY:
        case HFP_CONNECTING:
        case HFP_CONNECTED:
          memset(call, 0x00, sizeof(BT_CALL));
          call->state = status;
          break;
        case HFP_OUTGOIND:
        case HFP_INCOMING:
        case HFP_ACTIVE: {//todo: add multiple calls support, as it may kill an app
          call->idx = index;
          call->num.length = cmd->payload[offs];
          offs += 1;
          memcpy(&call->num.data[0], &cmd->payload[offs], call->num.length);
        } break;
      }
      return PostBack(UWM_NFORE_BT_HFP_STATE, index, (LPARAM)call);
    }
    case NFORE_BT_CMD_WAVE_OUT_CHANGE: {
      //todo: mute MMC microfone if call redirected to device
      return PostBack(UWM_NFORE_BT_VOICE_OUT_PATH, 0, status);
    }
    case NFORE_BT_CMD_CALL_PHONE_NUM_IN: {
      BYTE index = cmd->payload[offs];
      LPBT_CALL call = &g_calls[index];
      offs += 1;
      call->num.length = cmd->payload[offs];
      offs += 1;
      memcpy(&call->num.data[0], &cmd->payload[offs], call->num.length);
      return PostBack(UWM_NFORE_BT_HFP_STATE, status, (LPARAM)call);
    }
    case NFORE_BT_CMD_RSSI: {
      return PostBack(UWM_NFORE_BT_RSSI, 0, status);
    }
    case NFORE_BT_CMD_BATTERY_CHG: {
      return PostBack(UWM_NFORE_BT_BATTERY_CHG, 0, status);
    }
    case NFORE_BT_CMD_CALL_PHONE_NUM_OUT: {
      BYTE index = cmd->payload[offs];
      LPBT_CALL call = &g_calls[index];
      offs += 1+5;//5 bytes for unknown statuses
      call->num.length = cmd->payload[offs];
      offs += 1;
      memcpy(&call->num.data[0], &cmd->payload[offs], call->num.length);
      return PostBack(UWM_NFORE_BT_HFP_STATE, status, (LPARAM)call);
    }
    case NFORE_BT_CMD_A2DP_STATE: {
      return PostBack(UWM_NFORE_BT_A2DP_STATE, 0, status);
    }
    case NFORE_BT_CMD_AG_STATE: {
      return PostBack(UWM_NFORE_BT_AG_STATE, 0, status);
    }
    case NFORE_BT_CMD_AVRCP_STATE: {
      return PostBack(UWM_NFORE_BT_AVRCP_STATE, cmd->payload[offs], status);
    }
    case NFORE_BT_CMD_REMOTE_INFO: {
      BYTE index = cmd->payload[offs];
      LPBT_DEVICE dev = &g_devPaired[index];
      offs += 1;
      memcpy(&dev->MAC[0], &cmd->payload[offs], BT_MAC_LEN);
      offs += BT_MAC_LEN;
      dev->connected = TRUE;
      dev->name.length = cmd->payload[offs];
      offs += 1;
      memcpy(&dev->name.data[0], &cmd->payload[offs], dev->name.length);
      return PostBack(UWM_NFORE_BT_READ_REMOTE_INFO, index, (LPARAM)&g_devPaired);
    }
    case NFORE_BT_CMD_DISCONNECT: {
      g_devPaired[cmd->payload[0]].connected = FALSE;
      return PostBack(UWM_NFORE_BT_DISCONNECT_ACTION, cmd->payload[0], status);
    }
    case NFORE_BT_CMD_MUTE_STATE: {
      return PostBack(UWM_NFORE_BT_MUTE_STATE, 0, status);
    }
  }
  return FALSE;
}

LPBT_DEVICE GetDevByMAC (BT_DEVICE *list, size_t len, const BT_MAC MAC) {
  size_t i = 0;
  for (; i < len; i++) {
    if (0 == memcmp(MAC, &list[i].MAC, BT_MAC_LEN)) {
      return &list[i];
    }
  }
  return NULL;
}

BOOL PortWrite (BYTE* payload, size_t length) {
  BOOL fSuccess = FALSE;
  DWORD len = 0, res = 0;

  if(!g_hPort) {
    return FALSE;
  }
  EnterCriticalSection(&g_portWriteOperation);
  do {
    fSuccess = WriteFile(g_hPort, payload, length, &len, 0);  // ���������� �����
    res += len;
  } while (fSuccess && res < length);
  LeaveCriticalSection(&g_portWriteOperation);
  return fSuccess;
}

void LogCmd(LPNFORE_CMD cmd, LPCSTR prefix) {
  if (g_logFile) {
    CHAR buff[1024] = {0};
    CHAR packet[512] = {0};
    int i = 0;
    for (; i < cmd->length; i++) {
      sprintf(&packet[i*3], "%02x ", cmd->payload[i]);
    }
    sprintf(buff, "%d %s %02x\t%02x\t%s\r\n", GetTickCount(), prefix, cmd->code, cmd->length, packet);
    EnterCriticalSection(&g_logOperation);
    fwrite(buff, strlen(buff), 1, g_logFile);
    LeaveCriticalSection(&g_logOperation);
  }
}

void FetchDeviceInfo (LPBT_MAC mac) {
  NFORE_CMD cmd = {0};
  cmd.code = NFORE_BT_CMD_GET_DEVICE_INFO;
  cmd.length = BT_MAC_LEN;
  memcpy(&cmd.payload[0], mac, BT_MAC_LEN);
  cmd.waitCode = NFORE_BT_CMD_GET_DEVICE_INFO;
  SendCmd(&cmd);
}

void SearchContinue (LPBT_INQUERY_CoD cod) {
  NFORE_CMD cmd = {0};
  size_t i = 0;
  if (!g_searchInfinite) return;
  for (; i < BT_MAX_DEVICES; ++i) {
    g_devFound[i].refreshed = FALSE;
  }
  cmd.code = NFORE_BT_CMD_SEARCH_DEVICES;
  cmd.length = BT_INQUERY_COD_LEN;
  memcpy(&cmd.payload[0], cod, cmd.length);

  cmd.waitCode = NFORE_BT_CMD_SEARCH_DEVICES;
  SendCmd(&cmd);
}

int SendCmd (LPNFORE_CMD cmd) {
  EnterCriticalSection(&g_cmdQueOperation);
  memcpy(&g_cmdQue[g_cmdQueInPos], cmd, sizeof(NFORE_CMD));
  g_cmdQueInPos = (++g_cmdQueInPos)%BT_MAX_CMD_QUE;
  LeaveCriticalSection(&g_cmdQueOperation);
  SetEvent(g_eventNewCommand);//send new command event
  return g_cmdQueInPos;
}

#ifdef __cplusplus 
extern "C" {
#endif

NFORE_BT_API int BT_NFORE_Open (HWND hWnd) {
  if (!OpenPort()) {
    return NFORE_BT_ERR_PORT;
  }
  DllPortReadThread = CreateThread(NULL, 0, &PortReadThreadProc, NULL, 0, NULL);
  DllPortWriteThread = CreateThread(NULL, 0, &PortWriteThreadProc, NULL, 0, NULL);
  if (NULL == DllPortReadThread) {
    return NFORE_BT_ERR_THREAD;
  }
  g_HWND_dest = (NULL != hWnd) 
    ? hWnd 
    : HWND_BROADCAST;
  InitializeCriticalSection(&g_logOperation);
  InitializeCriticalSection(&g_portWriteOperation);
  InitializeCriticalSection(&g_cmdQueOperation);
  g_eventNewCommand = CreateEvent(NULL, FALSE, FALSE, NULL);
  g_eventCanContinue = CreateEvent(NULL, FALSE, FALSE, NULL);
  return NFORE_BT_ERR_NONE;
}

NFORE_BT_API int BT_NFORE_Close () {
  ClosePort();
  BT_NFORE_LogStop();
  CloseHandle(g_eventNewCommand);
  return NFORE_BT_ERR_NONE;
}

NFORE_BT_API int BT_NFORE_Reset () {
  NFORE_CMD cmd = {0};
  g_searchInfinite = FALSE;
  g_devFoundLen = 0;
  cmd.code = NFORE_BT_CMD_MODULE_RESET;
  cmd.length = 1;
  cmd.payload[0] = 0x01;
  cmd.waitCode = NFORE_BT_CMD_RESET_DONE;
  return SendCmd(&cmd);
}

NFORE_BT_API int BT_NFORE_LogStart (const char* fileName) {
  EnterCriticalSection(&g_logOperation);
  if (!g_logFile) {
    g_logFile = fopen(fileName, "wb");
  }
  LeaveCriticalSection(&g_logOperation);
  return NULL != g_logFile ? NFORE_BT_ERR_NONE : GetLastError();
}

NFORE_BT_API int BT_NFORE_LogStop () {
  EnterCriticalSection(&g_logOperation);
  if (g_logFile) {
    fflush(g_logFile);
    fclose(g_logFile);
    g_logFile = NULL;
  }
  LeaveCriticalSection(&g_logOperation);
  return NFORE_BT_ERR_NONE;
}

NFORE_BT_API int BT_NFORE_Init () {
  NFORE_CMD cmd = {0};
  cmd.code = NFORE_BT_CMD_MODULE_INFO;
  cmd.waitCode = NFORE_BT_CMD_MODULE_INFO;
  SendCmd(&cmd);
  cmd.code = NFORE_BT_CMD_MODULE_STATUS;
  cmd.waitCode = NFORE_BT_CMD_MODULE_STATUS;
  SendCmd(&cmd);
  return TRUE;
}

NFORE_BT_API int BT_NFORE_SendCmd (LPNFORE_CMD cmd) {
  return SendCmd(cmd);
}

NFORE_BT_API int BT_NFORE_SetVisible (BT_VISIBILITY v) {
  NFORE_CMD cmd = {0};
  cmd.code = NFORE_BT_CMD_SET_VISIBILITY;
  cmd.length = 1;
  cmd.payload[0] = v;
  cmd.waitCode = NFORE_BT_CMD_SET_VISIBILITY;
  return SendCmd(&cmd);
}

NFORE_BT_API int BT_NFORE_PairList () {
  NFORE_CMD cmd = {0};
  memset(&g_devPaired, 0x00, sizeof(g_devPaired));
  cmd.code = NFORE_BT_CMD_PAIR_DEVICE_LIST;
  cmd.waitCode = NFORE_BT_CMD_PAIR_DEVICE_LIST_END;
  return SendCmd(&cmd);
}

NFORE_BT_API int BT_NFORE_DevSearchStart (LPBT_INQUERY_CoD cod) {
  if (g_searchInfinite) return FALSE;
  g_devFoundLen = 0;
  memset(&g_devFound, 0x00, sizeof(g_devFound));
  g_searchInfinite = TRUE;
  SearchContinue(cod);
  return TRUE;
}

NFORE_BT_API int BT_NFORE_DevSearchStop () {
  g_searchInfinite = FALSE;
  return 0;
}

NFORE_BT_API int BT_NFORE_Connect (LPBT_MAC mac, BYTE index) {
  NFORE_CMD cmd = {0};
  cmd.code = NFORE_BT_CMD_PAIR_DEVICE_CONNECT;
  cmd.payload[0] = index;
  cmd.length += 1;
  cmd.waitCode = NFORE_BT_CMD_PAIR_DEVICE_CONNECT;
  if (NULL != mac) {
    memcpy(&cmd.payload[1], mac, BT_MAC_LEN);
    cmd.length += BT_MAC_LEN;
  }
  else {
    cmd.payload[1] = 0x09;
    cmd.length += 1;
  }
  return SendCmd(&cmd);
}

NFORE_BT_API int BT_NFORE_Pair (LPBT_MAC mac) {
  NFORE_CMD cmd = {0};
  cmd.code = NFORE_BT_CMD_PAIR_DEVICE;
  cmd.length = BT_MAC_LEN;
  memcpy(&cmd.payload[0], mac, BT_MAC_LEN);
  cmd.waitCode = NFORE_BT_CMD_PAIR_DEVICE;
  return SendCmd(&cmd);
}

NFORE_BT_API int BT_NFORE_SetPin (LPBT_PIN pin) {
  NFORE_CMD cmd = {0};
  cmd.code = NFORE_BT_CMD_SET_LOCAL_PIN;
  cmd.length = pin->length;
  memcpy(&cmd.payload[0], pin->data, cmd.length);
  cmd.waitCode = NFORE_BT_CMD_SET_LOCAL_PIN;
  return SendCmd(&cmd);
}

NFORE_BT_API int BT_NFORE_SetPinRq (BOOL rq) {
  NFORE_CMD cmd = {0};
  cmd.code = NFORE_BT_CMD_REQUIRE_PIN;
  cmd.length = 1;
  cmd.payload[0] = (BYTE)rq;
  cmd.waitCode = NFORE_BT_CMD_SET_LOCAL_PIN;
  return SendCmd(&cmd);
}

NFORE_BT_API LPBT_DEV_LIST BT_NFORE_GetPaired () {
  return &g_devPaired;
}

NFORE_BT_API LPBT_DEV_LIST BT_NFORE_GetFound () {
  return &g_devFound;
}

NFORE_BT_API int BT_NFORE_SendData (BYTE *buffer, size_t length) {
  NFORE_CMD cmd = {0};
  BYTE checksum = 0;
  size_t i;
  for (i = 0; i < length; i++) {
    checksum = checksum ^ buffer[i];
  }
  cmd.code = NFORE_BT_CMD_SPP;
  cmd.length = length+1;
  memcpy(&cmd.payload[0], buffer, length);
  cmd.payload[length] = checksum;
  cmd.waitCode = NFORE_BT_CMD_SET_LOCAL_PIN;
  return SendCmd(&cmd);
}

#ifdef __cplusplus 
}
#endif